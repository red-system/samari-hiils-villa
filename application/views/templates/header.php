<!DOCTYPE html>
<html lang="en">
<head>
    <!-- <?php 
     //header("Cache-Control: private, max-age=10800, pre-check=10800");
     //header("Pragma: private");
     //header("Expires: " . date(DATE_RFC822,strtotime("+7 day")));
    ?> -->
    <meta charset="utf-8">
    <!-- TITLE -->
    <title>Samari Hill Villas</title>
    <!-- <meta name="keywords" content="Bali villa, Bali  villas, Bali accommodation, Bali luxury villas, Bali pool villas, Private Villas, Bali villa rentals, Bali  rentals, Spa Services , Bali  holiday">
    <meta name="description" content="These luxury Bali villas are the have-it-all holiday accommodation for families and friends. "> -->
         

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/images/<?php echo $logofooter['main_image'];?>"/>

    <!-- GOOGLE FONT -->
    

    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/font-lotusicon.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/lib/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/helper.css">
    
    <!-- CSS LIBRARY -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-year-calendar-master/css/bootstrap-year-calendar.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-year-calendar-master/css/bootstrap-year-calendar.min.css">

    <!-- MAIN STYLE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css">
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMe_D6gusKtAYXmSwgO8iXFPlb4c0iOwU&sensor=false"></script>
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>

    <style>
    /* The Modal (background) */
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
    }

    /* The Close Button */
    .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    </style>
    
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
</head>

<!--[if IE 7]> <body class="ie7 lt-ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 8]> <body class="ie8 lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]> <body class="ie9 lt-ie10"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body onload="initialize()"> <!--<![endif]-->


    <!-- PRELOADER -->
    <div id="preloader">
        <span class="preloader-dot"></span>
    </div>
    <!-- END / PRELOADER -->

    <!-- PAGE WRAP -->
    <div id="page-wrap">

        <!-- HEADER -->
        <?php if (($this->session->menu=='home')== 'active') { ?>
            <header id="header" class="header-v2">
            <!-- HEADER TOP -->
            <div style="background-color: rgba(0,0,0,.7); width: 100%; height: 100%; position: absolute;z-index: -1"></div> 
            <div class="header_top">
                <div class="container">
                    <div class="header_left float-left">
                        <span><i class="lotus-icon-location"></i> <?php echo $address['general_data'];?></span>
                        <span><i class="lotus-icon-phone"></i> <a style="color: white" href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></span>
                        <span><i class="lotus-icon-phone"></i> <a style="color: white" href="tel:<?php echo $phone2['general_data'];?>"><?php echo $phone2['general_data'];?></a></span>
                        <span><i class="fa fa-envelope-o"></i> <a style="color: white" href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></span>
                    </div>
                    <div class="header_right float-right">
                        <div class="dropdown language">
                            <?php if ($lang == 'en') { ?>
                                 <span><img src="<?php echo base_url();?>assets/images/icon_United-Kingdom.png" ></span>
                            <?php } elseif ($lang == 'de') { ?>
                                <span><img src="<?php echo base_url();?>assets/images/icon_Germany.png" ></span>
                            <?php } ?>
                            
                            <ul style="min-width: 120px;">
                                <li class="<?php if( $lang == 'en') { echo 'active'; }  ?>">
                                    <?php echo anchor( $this->lang->switch_uri('en'),  img(array('src' => 'assets/images/icon_United-Kingdom.png')) .'&nbsp;English' ); ?>
                                </li>
                                <li class="<?php if( $lang == 'de') { echo 'active'; }  ?>">
                                    <?php echo anchor($this->lang->switch_uri('de'), img(array('src' => 'assets/images/icon_Germany.png')) .'&nbsp;German'); ?>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END / HEADER TOP -->
        <?php } 
            
            
        else {  ?>
        <header id="header" >
            <div style="background-color: rgba(0,0,0,.5); width: 100%; height: 100%; position: absolute;z-index: -1"></div>
            <div class="header_top">
                <div class="container">
                    <div class="header_left float-left">
                        <span><i class="lotus-icon-location"></i> <?php echo $address['general_data'];?></span>
                        <span><i class="lotus-icon-phone"></i> <a style="color: white" href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></span>
                        <span><i class="lotus-icon-phone"></i> <a style="color: white" href="tel:<?php echo $phone2['general_data'];?>"><?php echo $phone2['general_data'];?></a></span>
                        <span><i class="fa fa-envelope-o"></i> <a style="color: white" href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></span>
                    </div>
                    
                    <div class="header_right float-right">
                        <div class="dropdown language">
                            <?php if ($lang == 'en') { ?>
                                 <span><img src="<?php echo base_url();?>assets/images/icon_United-Kingdom.png" ></span>
                            <?php } elseif ($lang == 'de') { ?>
                                <span><img src="<?php echo base_url();?>assets/images/icon_Germany.png" ></span>
                            <?php } ?>
                            
                            <ul style="min-width: 120px;">
                                <li class="<?php if( $lang == 'en') { echo 'active'; }  ?>">
                                    <?php echo anchor( $this->lang->switch_uri('en'),  img(array('src' => 'assets/images/icon_United-Kingdom.png')) .'&nbsp;English' ); ?>
                                </li>
                                <li class="<?php if( $lang == 'de') { echo 'active'; }  ?>">
                                    <?php echo anchor($this->lang->switch_uri('de'), img(array('src' => 'assets/images/icon_Germany.png')) .'&nbsp;German'); ?>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END / HEADER TOP -->
        <?php } ?>
            
            <!-- HEADER LOGO & MENU -->
            <div class="header_content" id="header_content">

                <div class="container">
                    <!-- HEADER LOGO -->
                    <div class="header_logo">
                        <a href="<?php echo site_url($lang.'') ?>"><img src="<?php echo base_url();?>assets/images/<?php echo $logoheader['main_image'];?>" alt=""></a>
                    </div>
                    <!-- END / HEADER LOGO -->
                    
                    <!-- HEADER MENU -->
                    <nav class="header_menu">
                        <ul class="menu">
                            <li class="<?php if($this->session->menu=='home'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'') ?>"><?php echo $home['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='the_villa'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/the-villa') ?>"><?php echo $thevilla['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='location'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/location') ?>"><?php echo $location['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='offers'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/offers') ?>"><?php echo $offers['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='gallery'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/gallery') ?>"><?php echo $gallery['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='availability'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/availability') ?>"><?php echo $availability['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='guest_book'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/guest-book') ?>"><?php echo $guestbook['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='pricing'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/pricing') ?>"><?php echo $pricing['title'];?></a></li>
                            <li class="<?php if($this->session->menu=='contact'){echo('current-menu-item');} ?>"><a href="<?php echo site_url($lang.'/contact') ?>"><?php echo $contact['title'];?></a></li>
                        </ul>
                    </nav>
                    <!-- END / HEADER MENU -->

                    <!-- MENU BAR -->
                    <span class="menu-bars">
                        <span></span>
                    </span>
                    <!-- END / MENU BAR -->

                </div>
            </div>
            <!-- END / HEADER LOGO & MENU -->

        </header>
        <!-- END / HEADER -->