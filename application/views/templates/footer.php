<!-- FOOTER -->
<style>

/*--------------------------------Button starts--------------------------------*/
.button-container {
  text-align: center;
}
.button {
  position: relative;
  background: currentColor;
  border: 1px solid currentColor;
  font-size: 16px;
  color: #e1bd85;
  margin: 3rem 0;
  padding: 0.75rem 3rem;
  cursor: pointer;
  -webkit-transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  transition: background-color 0.28s ease, color 0.28s ease, box-shadow 0.28s ease;
  overflow: hidden;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}
.button span {
  color: #fff;
  
}
.button:hover {
  color: #f4e1a1;
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.2);
}
.button:active::before, .button:focus::before {
  -webkit-transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s;
  transition: transform 1.12s ease, opacity 0.28s ease 0.364s, -webkit-transform 1.12s ease;
  -webkit-transform: translate(-50%, -50%) scale(1);
          transform: translate(-50%, -50%) scale(1);
  opacity: 0;
}
.button:focus {
  outline: none;
}
/*--------------------------------Button ends--------------------------------*/
</style>
        <footer id="footer" style="background-color: #5d5d5d">

            <!-- FOOTER TOP -->
            <div class="footer_top">
                <div class="container">
                    <div class="row">

                        <!-- WIDGET MAILCHIMP -->
                        <div class="col-lg-6">
                            <div class="mailchimp">
                                <h4 style="color:#fff; font-size: 23px;" ><?php echo $quote['general_desc'];?></h4><br>
                            </div>
                            
                        </div>
                        <div class="col-lg-3 text-center" style="margin-top: 1%">
                            <a type="button" class="button" href="<?php echo site_url($lang.'/contact') ?>"><span><?php echo $button['general_data'];?></span></a>
                        </div>
                        <!-- END / WIDGET MAILCHIMP -->
                        
                        <!-- WIDGET SOCIAL -->
                        <div class="col-lg-3">
                            <div class="social">
                                <div class="social-content">
                                    <?php foreach ($social_medias as $social_media) :  ?>
                                        <a target="_blank" href="<?php echo $social_media['social_link'];?>"><i class="<?php echo $social_media['social_logo'];?>"></i></a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <!-- END / WIDGET SOCIAL -->

                    </div>
                </div>
            </div>
            <!-- END / FOOTER TOP -->

            <!-- FOOTER CENTER -->
            <div class="footer_center">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-lg-6">
                            <div class="widget widget_logo">
                                <div class="widget-logo">
                                    <div class="img">
                                        <a href="#"><img src="<?php echo base_url();?>assets/images/<?php echo $logofooter['main_image'];?>" alt=""></a>
                                    </div>
                                    <div class="text">
                                        <p style="color: black"><i class="lotus-icon-location"></i> <?php echo $address['general_data'];?></p>
                                        <p style="color: black"><i class="lotus-icon-phone"></i> <a style="color: black" href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></p>
                                        <p style="color: black"><i class="lotus-icon-phone"></i> <a style="color: black" href="tel:<?php echo $phone2['general_data'];?>"><?php echo $phone2['general_data'];?></a></p>
                                        <p style="color: black"><i class="fa fa-envelope-o"></i> <a style="color: black" href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-xs-4 col-lg-3">
                            <div class="widget widget_tripadvisor">
                                <h4 class="widget-title">Tripadvisor</h4>
                                <div class="tripadvisor">
                                    <p>Now with hotel reviews by</p>
                                    <img src="<?php echo base_url();?>assets/images/tripadvisor.png" alt="">
                                    <span class="tripadvisor-circle">
                                        <i></i>
                                        <i></i>
                                        <i></i>
                                        <i></i>
                                        <i class="part"></i>
                                    </span>
                                </div>
                            </div>
                        </div> -->


                    </div>
                </div>
            </div>
            <!-- END / FOOTER CENTER -->

            <!-- FOOTER BOTTOM -->
            <div class="footer_bottom">
                <div class="container">
                    <p>Copyright &copy; <?php echo date ('Y');?> <a href="http://samarihillvillas.com" target="_blank" style="color: white;">Samari Hill Villas</a> |  Template by <a href="http://cortechstudio.com" target="_blank" style="color: white;">Cortech Studio</a> | All rights reserved.</p>
                </div>
            </div>
            <!-- END / FOOTER BOTTOM -->

        </footer>
        <!-- END / FOOTER -->

    </div>
    <!-- END / PAGE WRAP -->


    <!-- LOAD JQUERY -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/bootstrap-select.js"></script>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMe_D6gusKtAYXmSwgO8iXFPlb4c0iOwU&sensor=false"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/owl.carousel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.appear.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.countTo.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lib/SmoothScroll.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/scripts.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-year-calendar-master/js/bootstrap-year-calendar.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-year-calendar-master/js/bootstrap-year-calendar.min.js"></script>

    

</body>
</html>