<!-- BANNER SLIDER -->
<section class="section-slider">
    <h1 class="element-invisible">Slider</h1>
    <div id="slider-revolution" >
        <ul>
            <li data-transition="fade" >
                <img src="<?php echo base_url();?>assets/images/slider/<?php echo $picture['main_image'];?>" data-bgposition="left center" data-duration="14000" data-bgpositionend="right center" alt="">
                
                <div class="tp-caption sft fadeout slider-caption-sub slider-caption-1" data-x="center" data-y="240" data-speed="700" data-start="1500" data-easing="easeOutBack">
                <img src="<?php echo base_url();?>assets/images/slider/<?php echo $icon['main_image'];?>" alt="icons">
                </div>
                <div class="tp-caption sft fadeout slider-caption-sub slider-caption-1" data-x="center" data-y="390" data-speed="700" data-start="1500" data-easing="easeOutBack">
                 <?php echo $homepicture['general_data'];?>
                </div>

                <div class="tp-caption sfb fadeout slider-caption slider-caption-sub-1" data-x="center" data-y="420" data-speed="700" data-easing="easeOutBack"  data-start="2000"><?php echo $homepicture['general_sub_data'];?></div>
            </li> 
           
        </ul>
    </div>

</section>
<!-- END / BANNER SLIDER -->

<!-- ACCOMMODATIONS -->
<section class="section-accommo_1 bg-white">
    <div class="container">

        <div class="accomd-modations_1">

            <div class="accomd-modations-content_1" >

                <div class="accomd-modations-slide_1" >

                    
                        <!-- ITEM -->
                        <div class="accomd-modations-room_1">
                        
                            <div class="img">
                                <a href="<?php echo base_url();?>location"><img src="<?php echo base_url();?>assets/images/home/<?php echo $image1['picture_name'];?>" alt=""></a>
                            </div>
                        
                            <div class="text" style="margin-top: 0px;">
                               <div class="wrap-price">
                                    <p class="price" style="color: black">
                                       <?php echo $menulink1['general_data'];?>
                                    </p>
                                    <a href="<?php echo site_url($lang.'/'.$menulink1["general_link"]) ?>" class="awe-btn awe-btn-default" style="padding: 0 1px;line-height: 45px;"><?php echo $viewdetail['title'];?></a>
                                </div>
                            </div>
                        
                        </div>
                        <!-- END / ITEM -->

                        <!-- ITEM -->
                        <div class="accomd-modations-room_1">
                        
                            <div class="img">
                                <a href="<?php echo base_url();?>location"><img src="<?php echo base_url();?>assets/images/home/<?php echo $image2['picture_name'];?>" alt=""></a>
                            </div>
                        
                            <div class="text" style="margin-top: 0px;">
                               <div class="wrap-price">
                                    <p class="price" style="color: black">
                                       <?php echo $menulink2['general_data'];?>
                                    </p>
                                    <a href="<?php echo site_url($lang.'/'.$menulink2["general_link"]) ?>" class="awe-btn awe-btn-default" style="padding: 0 1px;line-height: 45px;"><?php echo $viewdetail['title'];?></a>
                                </div>
                            </div>
                        
                        </div>
                        <!-- END / ITEM -->

                        <!-- ITEM -->
                        <div class="accomd-modations-room_1">
                        
                            <div class="img">
                                <a href="<?php echo base_url();?>location"><img src="<?php echo base_url();?>assets/images/home/<?php echo $image3['picture_name'];?>" alt=""></a>
                            </div>
                        
                            <div class="text" style="margin-top: 0px;">
                               <div class="wrap-price">
                                    <p class="price" style="color: black">
                                       <?php echo $menulink3['general_data'];?>
                                    </p>
                                    <a href="<?php echo site_url($lang.'/'.$menulink3["general_link"]) ?>" class="awe-btn awe-btn-default" style="padding: 0 1px;line-height: 45px;"><?php echo $viewdetail['title'];?></a>
                                </div>
                            </div>
                        
                        </div>
                        <!-- END / ITEM -->
                    
                    
                </div>

            </div>

        </div>

    </div>
</section>
<!-- ACCOMMODATIONS -->

<!-- SECTION GUESTBOOK - EVENT DEAD -->
<section class="section-guestbook-event bg-white">
    <div class="container">

        <div class="guestbook-event">
            <div class="row">

                <div class="col-md-6">

                    <h2 class="heading"><?php echo $guestbook['title'];?></h2>
                    <div class="guestbook-content_1 owl-single">
                        <?php foreach ($guestbooklists as $guestbooklist) :  ?>
                            <!-- ITEM -->
                            <div class="guestbook-item_1">
                                <div class="img">
                                    <img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $guestbooklist['main_image'];?>" alt="">
                                    <span><strong><?php echo $guestbooklist['general_data'];?></strong><?php echo $guestbooklist['general_link'];?></span>
                                </div>
                            
                                <div class="text">
                                    <p><?php echo $guestbooklist['general_sub_desc'];?></p>
                                </div> 
                            </div>
                            <!-- END / ITEM -->
                        <?php endforeach; ?>
                        

                    </div>

                </div>

                <div class="col-md-6">
                    <h2 class="heading"><?php echo $guestbook['title'];?></h2>

                    <div class="event-slide owl-single">
                        <?php foreach ($guestbookimages as $pic):?>
                        <!-- ITEM -->
                        <div class="event-item">
                            <div class="img hover-zoom">
                                <div class="item-isotope  ground suite dining">
                                    <div class="gallery_item">
                                        <a href="<?php echo base_url();?>assets/images/guestbook/<?php echo $pic['picture_name'];?>" class="mfp-image">
                                            <img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $pic['picture_name'];?>" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END / ITEM -->
                        <?php endforeach;?>
                        

                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- END / SECTION GUESTBOOK - EVENT DEAD -->

<!-- ABOUT -->
<section class="section-home-about bg-white">
    <div class="container">
        <div class="about">

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imageaboutus as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/home/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/home/<?php echo $picabout['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>

                    
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h2 class="heading"><?php echo $aboutus['general_data'];?></h2>
                    <div class="desc">
                        <p><?php echo $aboutus['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            

        </div>
    </div>
</section>
<!-- END / ABOUT -->

<!-- OUR BEST -->
<section class="section-our-best bg-white">
    <div class="container">

        <div class="our-best">
            <div class="row">

                <div class="col-md-6 col-md-push-6">
                    <?php foreach ($imageourbest as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/home/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/home/<?php echo $picabout['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="col-md-6 col-md-pull-6 ">
                    <div class="text">
                        <h2 class="heading"><?php echo $ourbest['general_data'];?></h2>
                        <p><?php echo $ourbest['general_desc'];?></p>
                        <ul>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg1['picture_name'];?>" alt="icon"><?php echo $feature1['general_data'];?></li>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg2['picture_name'];?>" alt="icon"><?php echo $feature2['general_data'];?></li>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg3['picture_name'];?>" alt="icon"><?php echo $feature3['general_data'];?></li>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg4['picture_name'];?>" alt="icon"><?php echo $feature4['general_data'];?> </li>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg5['picture_name'];?>" alt="icon"><?php echo $feature5['general_data'];?></li>
                            <li><img src="<?php echo base_url();?>assets/images/home/<?php echo $featureimg6['picture_name'];?>" alt="icon"><?php echo $feature6['general_data'];?></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>
<!-- END / OUR BEST -->