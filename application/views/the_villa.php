<style type="text/css">

img {
  max-width: 100%;
  height: auto;
}

.map {
  position: relative;
  max-width: 1100px;
  margin: auto;
  background-color: #ccc;
}
.map img {
  display: block;
  width: 100%;
}

.map-popup {
  position: absolute;
  left: 50%;
  top: 130%;
  z-index: 99;
  width: 60%;
  padding: 2rem;
  background-color: #fff;
  transition: all 300ms ease-in;
  transform: translate(-50%,-50%);
  box-shadow: 0 0 24px rgba(0,0,0,0.22);
  opacity: 0;
  visibility: hidden;
}

.map-popup > *:first-child { margin-top: 0; }
.map-popup > *:last-child { margin-bottom: 0; }

.map-popup.open {
  opacity: 1;
  visibility: visible;
}

.marker {
  position: absolute;
  z-index: 1;
  display: inline-block;
  width: 20px;
  height: 20px;
  color: #fff;
  line-height: 20px;
  text-align: center;
  text-decoration: none;
  border-radius: 50%;
  background-color: tomato;
  transition: all 300ms;
}

.marker1 {
  top: 20%;
  left: 8%;
}
.marker2 {
  top: 15%;
  left: 24%;
}
.marker3 {
  top: 49%;
  left: 18%;
}
.marker4 {
  top: 50%;
  left: 75%;
}

@media only screen and (min-width:48em) {
  .map-item {
    position: absolute;
    width: 36px;
    height: 36px;
    }
    .map-item .marker {
      top: auto;
      left: auto;
      width: 36px;
      height: 36px;
      line-height: 36px;
      text-align: center;
    }
  
  .map-item1 {
    top: 23%;
    left: 10%;
  }
  .map-item2 {
    top: 18%;
    left: 27%;
  }
  .map-item3 {
    top: 57%;
    left: 25%;
  }
  .map-item4 {
    top: 50%;
    left: 78%;
  }
  
  .map-popup {
    position: absolute;
    left: 58px;
    width: 280px;
    transform: translateY(-50%);
  }
  .map-popup:before {
    content: "";
    position: absolute;
    top: 18%;
    left: -16px;
    margin-top: -16px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 16px 16px 16px 0;
    border-color: transparent #fff transparent transparent;
  }
  
  .map-popup.edge {
    left: auto;
    right: calc(100% + 24px);
  }
  .map-popup.edge:before {
    left: auto;
    right: -16px;
    border-width: 16px 0 16px 16px;
    border-color: transparent transparent transparent #fff;
  }
}
</style>

<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- ABOUT -->
<section class="section-about">
    <div class="container">

        <div class="about">

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imageabout as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $picabout['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>

                    
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h2 class="heading"><?php echo $about['general_data'];?></h2>
                    <div class="desc">
                        <p><?php echo $about['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagearea as $picarea):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $picarea['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $picarea['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h2 class="heading"><?php echo $area['general_data'];?></h2>
                    <div class="desc">
                        <p><?php echo $area['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imagebuilding as $picbuilding):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $picbuilding['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $picbuilding['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h2 class="heading"><?php echo $building['general_data'];?></h2>
                    <div class="desc">
                        <p><?php echo $building['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagecomfort as $piccomfort):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/about/<?php echo $piccomfort['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/about/<?php echo $piccomfort['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h2 class="heading"><?php echo $comfort['general_data'];?></h2>
                    <div class="desc">
                        <p><?php echo $comfort['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

        </div>

    </div>
</section>
<!-- END / ABOUT -->

<section class="section-team">
    <div class="container">

      <div class="text">
        <h2 class="heading text-center"><?php echo $groundplan['title'];?></h2><br><br>
        <div class="map">
  
          <div class="map-item map-item1">
            <a class="marker marker1" href="#marker1">1</a>

            <aside id="marker1" class="map-popup">
              <h3 class="popup-title">Sleeping</h3>
              <p>17,10 m<sup>2</sup></p>
            </aside>
          </div>

          <div class="map-item map-item2">
            <a class="marker marker2" href="#marker2">2</a>

            <aside id="marker2" class="map-popup">
              <h3 class="popup-title">Bathroom</h3>
              <p>90,8 m<sup>2</sup></p>
            </aside>
          </div>
          
          <div class="map-item map-item3">
            <a class="marker marker3" href="#marker3">3</a>

            <aside id="marker3" class="map-popup">
              <h3 class="popup-title">Living</h3>
              <p>44,5 m<sup>2</sup></p>
            </aside>
          </div>

          <div class="map-item map-item4">
            <a class="marker marker4" href="#marker4">4</a>

            <aside id="marker4" class="map-popup">
              <h3 class="popup-title">Pool</h3>
              <p>21,75 m<sup>2</sup></p>
            </aside>
          </div>
            
          <img src="<?php echo base_url();?>assets/images/Untitled.jpg" alt="">
        </div>

      </div>
    </div>
</section>
<!-- END / TEAM -->

<!-- TEAM -->
<section class="section-team">
    <div class="container">

        <div class="team">
            <h2 class="heading text-center"><?php echo $teammember['title'];?></h2>
            
            <div class="team_content">
                <div class="row">
                    <?php foreach ($teams as $team):?>
                        <!-- ITEM -->
                        <div class="col-xs-6 col-md-3">
                            <div class="team_item text-center">

                                <div class="img">
                                    <a href=""><img src="<?php echo base_url();?>assets/images/team/<?php echo $team['main_image'];?>" alt=""></a>
                                </div> 

                                <div class="text">
                                    <h2><?php echo $team['general_data'];?></h2>
                                    <span><?php if( $lang == 'de') { echo $team['general_sub_desc']; } else  if ( $lang == 'en' ) { echo $team['general_desc']; } ?></span>
                                    <div class="team-share">
                                        <a href="mailto:<?php echo $team['email_link'];?>" target="_blank"><i class="fa fa-envelope"></i></a>
                                        <a href="<?php echo $team['facebook_link'];?>" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="<?php echo $team['twitter_link'];?>" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="<?php echo $team['instagram_link'];?>" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- END / ITEM -->
                    <?php endforeach;?>
                    

                </div>
            </div>
        </div>

    </div>
</section>
<!-- END / TEAM -->

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
<script type="text/javascript">
    jQuery(function($) {
      var pop = $('.map-popup');
      pop.click(function(e) {
        e.stopPropagation();
      });
      
      $('a.marker').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).next('.map-popup').toggleClass('open');
        $(this).parent().siblings().children('.map-popup').removeClass('open');
      });
      
      $(document).click(function() {
        pop.removeClass('open');
      });
      
      pop.each(function() {
        var w = $(window).outerWidth(),
            edge = Math.round( ($(this).offset().left) + ($(this).outerWidth()) );
        if( w < edge ) {
          $(this).addClass('edge');
        }
      });
    });
</script>
