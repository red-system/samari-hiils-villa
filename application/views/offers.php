<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->
<!-- ABOUT -->
<section class="section-about">
    <div class="container">

        <div class="about">

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imagefirst as $picfirst):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfirst['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfirst['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                   
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h3><strong> <?php echo $first['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $first['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagesecond as $picsecond):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picsecond['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picsecond['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $second['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $second['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imagethird as $picthird):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picthird['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picthird['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $third['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $third['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagefourth as $picfourth):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfourth['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfourth['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $fourth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $fourth['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imagefifth as $picfifth):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picfifth['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picfifth['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $fifth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $fifth['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagesixth as $picsixth):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picsixth['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picsixth['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $sixth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $sixth['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imageseventh as $picseventh):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $picseventh['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $picseventh['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $seventh['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $seventh['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imageeighth as $piceighth):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/offers/<?php echo $piceighth['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/offers/<?php echo $piceighth['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $eighth['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $eighth['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            
        </div>

    </div>
</section>
<!-- END / ABOUT -->

