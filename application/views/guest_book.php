<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->
<?php 
   if($this->session->flashdata('true')){
 ?>
   <div class="alert alert-success"> 
     <?php  echo $this->session->flashdata('true'); ?>
    </div>
<?php    
}else if($this->session->flashdata('err')){
?>
 <div class = "alert alert-success">
   <?php echo $this->session->flashdata('err'); ?>
 </div>
<?php } ?>
<!-- GUEST BOOK -->
<section class="section-guest-book">
    <div class="container">
        <div class="guest-book">
            
            <!-- GUEST BOOK HEAD -->
            <div class="guest-book_head bg-8" style="background: url(<?php echo base_url();?>assets/images/guestbook/img-8.jpg) no-repeat firebrick;  ">
                <div class="text">
                    <h2><?php echo $about['general_data'];?></h2>
                    <p><?php echo $about['general_desc'];?></p>
                    <button id="myBtn" class="awe-btn awe-btn-13"><?php echo $about['general_sub_desc'];?></button>
                </div>
            </div>
            <!-- END / GUEST BOOK HEAD -->

            <!-- GUEST BOOK MASONRY -->
            <div class="guest-book_content">

                <div class="row">
                    <div class="guest-book_mansory">
                        <?php foreach ($data->result() as $guestbooklist) :  ?>
                            <!-- ITEM -->
                            <div class="item-masonry col-xs-6 col-md-4">
                                <div class="guest-book_item guest-book_item-2">
                                    <span class="icon lotus-icon-quote-left"></span>
                                    <div class="avatar">
                                        <img src="<?php echo base_url();?>assets/images/guestbook/<?php echo $guestbooklist->main_image;?>" alt="">
                                    </div>
                                    <h2><?php echo $guestbooklist->general_desc;?></h2>
                                    <p><?php echo $guestbooklist->general_sub_desc;?></p>
                                    <span><b><?php echo $guestbooklist->general_data;?></b> - <?php echo $guestbooklist->general_link;?></span>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        <?php endforeach; ?>
                    </div>
                </div>

                <!-- PAGE NAVIGATION   -->
                <?php echo $pagination; ?>
                <!-- END / PAGE NAVIGATION   -->

            </div>
            <!-- END / GUEST BOOK MASONRY -->
        </div>
    </div>
</section>
<!-- END / GUEST BOOK -->

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
    <div class="modal-content">
        <span class="close" >&times;</span>
        <h2><?php echo $modal['general_data'];?></h2>
        <p><?php echo $modal['general_desc'];?></p>
        <?php echo form_open_multipart($lang.'/guest-book/guestbook-addprocess','class="form-horizontal tasi-form"'); ?>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="<?php echo $modal['general_sub_data'];?>" name="name" required="" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="<?php echo $modal['general_sub_desc'];?>" name="location" required="" />
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="<?php echo $modal['general_link'];?>" name="title" required="" />
            </div>
            <div class="form-group">
                <textarea class="form-control" placeholder="<?php echo $modal['main_image'];?>" name="testimonial" required=""></textarea>
            </div>
            <div class="form-group">
                <label class="col-sm-2 "><?php echo $modal['secondary_image'];?></label> <input name="picture" type="file" class="form-control" placeholder="Profil Picture">
            </div>
            <div class="form-field text-center">
                <button class="awe-btn awe-btn-13" type="submit" name="action"><?php echo $modal['general_password'];?></button>
            </div>
        </form>
    </div>
</div>

<script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
      modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
      modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
    </script>