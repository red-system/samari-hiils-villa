<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<section class="section-about">
    <div class="container">

        <div class="about">

            <!-- ITEM -->
            <div class="about-item ">

                <div class="img owl-single">
                    <?php foreach ($imagefirst as $picfirst):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/pricing/<?php echo $picfirst['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/pricing/<?php echo $picfirst['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                   
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h3><strong> <?php echo $first['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $first['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagesecond as $picsecond):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/pricing/<?php echo $picsecond['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/pricing/<?php echo $picsecond['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                   
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h3><strong> <?php echo $second['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $second['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            

        </div>
        
        <!-- ITEM -->
        <div class="col-md-6">

            <div class="text">
                <br><h3><strong> <?php echo $pricinglist['general_data'];?></strong></h3><br>
                <div class="desc">
                  	<a href="<?php echo base_url();?>assets/<?php echo $pricinglist['main_image'];?>" class="mfp-image">
                         <img src="<?php echo base_url();?>assets/images/icons-pdf.png" alt="">
                    </a> 
                </div>
            </div>

        </div>
        <!-- END / ITEM -->

    </div>
</section>
<!-- END / ABOUT -->




            

            

      