<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
                  <li>
                      <a class="<?php if($this->session->menu=='backend'){echo('active');} ?>" href="<?php echo base_url();?>backend">
                          <i class="icon-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php if($this->session->menu=='backend_headerfooter'){echo('active');} ?>" href="<?php echo base_url();?>backend_headerfooter">
                          <i class="icon-laptop"></i>
                          <span>Header & Footer</span>
                      </a>
                  </li>
                  <li>
                      <a class="<?php if($this->session->menu=='backend_menutitle'){echo('active');} ?>" href="<?php echo base_url();?>backend_menu_title">
                          <i class="icon-reorder"></i>
                          <span>Menu & Title Template</span>
                      </a>
                  </li>
                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_homepicture'||$this->session->menu=='backend_menulink1'||$this->session->menu=='backend_menulink2'||$this->session->menu=='backend_menulink3'||$this->session->menu=='backend_guestbookgallery'||$this->session->menu=='backend_aboutus'||$this->session->menu=='backend_ourbest'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-h-sign"></i>
                          <span>Home</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_homepicture'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/homepicture">Home Picture</a></li>
                          <li class="sub-menu">
                              <a class="<?php if($this->session->menu=='backend_menulink1'||$this->session->menu=='backend_menulink2'||$this->session->menu=='backend_menulink3'){echo('active');} ?>" href="javascript:;" >
                                <span>Menu Link</span>
                              </a>
                              <ul class="sub">
                                  <li class="<?php if($this->session->menu=='backend_menulink1'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/menulink1">Menu Link 1</a></li>
                                  <li class="<?php if($this->session->menu=='backend_menulink2'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/menulink2">Menu Link 2</a></li>
                                  <li class="<?php if($this->session->menu=='backend_menulink3'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/menulink3">Menu Link 3</a></li>
                              </ul>
                          </li>
                          <li class="<?php if($this->session->menu=='backend_guestbookgallery'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/guestbook_gallery">Guest Book Gallery</a></li>
                          <li class="<?php if($this->session->menu=='backend_aboutus'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/about_us">About Us</a></li>
                          <li class="<?php if($this->session->menu=='backend_ourbest'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_home/our_best">Our Best</a></li>
                    </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheadervilla'||$this->session->menu=='backend_aboutvilla'||$this->session->menu=='backend_thearea'||$this->session->menu=='backend_thebuilding'||$this->session->menu=='backend_comfort'||$this->session->menu=='backend_teammember'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-home"></i>
                          <span>The Villa</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheadervilla'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/header_villa">Page Header</a></li>
                      </ul>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_aboutvilla'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/about_villa">About Villa</a></li>
                          <li class="<?php if($this->session->menu=='backend_thearea'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/the_area">The Area</a></li>
                          <li class="<?php if($this->session->menu=='backend_thebuilding'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/the_building">The Building</a></li>
                          <li class="<?php if($this->session->menu=='backend_comfort'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/comfort">Comfort</a></li>
                          <li class="<?php if($this->session->menu=='backend_teammember'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_thevilla/team_member">Team Member</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheaderlocation'||$this->session->menu=='backend_aboutlocation'||$this->session->menu=='backend_map'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-map-marker"></i>
                          <span>Location</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheaderlocation'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_location/header_location">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_aboutlocation'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_location/about_location">About Location</a></li>
                          <li class="<?php if($this->session->menu=='backend_condition'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_location/condition">Condition</a></li>
                          <li class="<?php if($this->session->menu=='backend_map'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_location/map">Map</a></li>
                      </ul>
                  </li>
                  
                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheaderoffers'||$this->session->menu=='backend_offers1'||$this->session->menu=='backend_offers2'||$this->session->menu=='backend_offers3'||$this->session->menu=='backend_offers4'||$this->session->menu=='backend_offers5'||$this->session->menu=='backend_offers6'||$this->session->menu=='backend_offers7'||$this->session->menu=='backend_offers8'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-certificate"></i>
                          <span>Offers</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheaderoffers'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/header_offers">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers1'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_first">Offers First</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers2'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_second">Offers Second</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers3'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_third">Offers Third</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers4'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_fourth">Offers Fourth</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers5'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_fifth">Offers Fifth</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers6'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_sixth">Offers Sixth</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers7'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_seventh">Offers Seventh</a></li>
                          <li class="<?php if($this->session->menu=='backend_offers8'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_offers/offers_eighth">Offers Eighth</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheadergallery'||$this->session->menu=='backend_gallery1'||$this->session->menu=='backend_gallery2'||$this->session->menu=='backend_gallerythevilla'||$this->session->menu=='backend_galleryfacets'||$this->session->menu=='backend_galleryadditional'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-folder-open"></i>
                          <span>Gallery</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheadergallery'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/header_gallery">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_gallery1'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/first_view">First View</a></li>
                          <li class="<?php if($this->session->menu=='backend_gallery2'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/second_view">Second View</a></li>
                          <li class="<?php if($this->session->menu=='backend_gallerythevilla'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/the_villa">The Villa</a></li>
                          <li class="<?php if($this->session->menu=='backend_galleryfacets'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/facets">Facets</a></li>
                          <li class="<?php if($this->session->menu=='backend_galleryadditional'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_gallery/additional">Additional</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheaderavailability'||$this->session->menu=='backend_reservation'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-cloud-download"></i>
                          <span>Availability</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheaderavailability'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_availability/header_availability">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_reservation'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_availability/reservation">Reservation</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheaderguestbook'||$this->session->menu=='backend_guestbookabout'||$this->session->menu=='backend_guestbookform'||$this->session->menu=='backend_guestbooklist'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-book"></i>
                          <span>Guestbook</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheaderguestbook'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_guestbook/header_guestbook">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_guestbookabout'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_guestbook/about_guestbook">About Guestbook</a></li>
                          <li class="<?php if($this->session->menu=='backend_guestbookform'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_guestbook/guestbook_form">Guestbook Form</a></li>
                          <li class="<?php if($this->session->menu=='backend_guestbooklist'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_guestbook/guestbook_list">Guestbook List</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheaderpricing'||$this->session->menu=='backend_firstfeeling'||$this->session->menu=='backend_secondfeeling'||$this->session->menu=='backend_pricelist'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-usd"></i>
                          <span>Pricing</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheaderpricing'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_pricing/header_pricing">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_firstfeeling'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_pricing/first_feeling">First Feeling</a></li>
                          <li class="<?php if($this->session->menu=='backend_secondfeeling'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_pricing/second_feeling">Second Feeling</a></li>
                          <li class="<?php if($this->session->menu=='backend_pricelist'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_pricing/pricelist">Pricelist</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a class="<?php if($this->session->menu=='backend_pageheadercontact'||$this->session->menu=='backend_content'){echo('active');} ?>" href="javascript:;" >
                          <i class="icon-phone-sign"></i>
                          <span>Contact</span>
                      </a>
                      <ul class="sub">
                          <li class="<?php if($this->session->menu=='backend_pageheadercontact'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_contact/header_contact">Page Header</a></li>
                          <li class="<?php if($this->session->menu=='backend_content'){echo('active');} ?>"><a  href="<?php echo base_url();?>backend_contact/content">Content</a></li>
                      </ul>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->