<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend <?php echo $title;?></li>
              </ul>
              <!--breadcrumbs end -->
          </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Form Backend <?php echo $title;?>
                        <span class="tools pull-right">
                          <a href="javascript:;" class="icon-chevron-down"></a> 
                        </span>
                    </header>
                    <div class="panel-body">
                      <?php 
                         if($this->session->flashdata('true')){
                       ?>
                         <div class="alert alert-success"> 
                           <?php  echo $this->session->flashdata('true'); ?>
                          </div>
                      <?php    
                      }else if($this->session->flashdata('err')){
                      ?>
                       <div class = "alert alert-success">
                         <?php echo $this->session->flashdata('err'); ?>
                       </div>
                      <?php } ?>
                      <div class="adv-table">
                        <div class="adv-table">
                          <div class="clearfix">
                            <div class="btn-group pull-right">
                                <a  class="btn btn-default" href="<?php echo base_url();?>backend_availability/add_reservation">
                                   <i class="icon-plus-sign"> </i> Add Reservation
                                </a>
                            </div>
                          </div>
                        <div class="space15"></div> <br>
                        <table  class="display table table-bordered table-striped" id="example">
                          <thead>
                          <tr>
                            <th width="15%">Name</th>
                            <th width="10%">Email</th>
                            <th width="10%">Phone</th>
                            <th width="10%">Arrival</th>
                            <th width="10%">Departure</th>
                            <th width="10%">Guest</th>
                            <th width="15%">Message</th>
                            <th width="5%">Edit</th>
                            <th width="5%">Delete</th>
                          </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($reservations as $reservation) :  ?>
                              <tr class="gradeX">
                                  <td><?php echo $reservation['name'];?></td>
                                  <td><?php echo $reservation['email'];?></td>
                                  <td><?php echo $reservation['phone'];?></td>
                                  <td><?php echo $reservation['start'];?></td>
                                  <td><?php echo $reservation['end'];?></td>
                                  <td><?php echo $reservation['guest'];?></td>
                                  <td><?php echo word_limiter($reservation['message'],15); ?></td>
                                  <td class="text-center"><a class="btn btn-round btn-primary" title="view & edit" href="<?php echo site_url('backend_availability/'.$link.'/'.$reservation['id_reservasi']); ?>" type="button"><i class="icon-pencil"></i></a></td>
                                  <td class="text-center"><a class="btn btn-round btn-danger" title="delete" href="<?php echo site_url('backend_availability/'.$link1.'/'.$reservation['id_reservasi']); ?>" onclick="return confirm('Are you sure to delete?')" type="button"><i class="icon-trash"></i></a></td>
                              </tr>
                            <?php endforeach; ?>
                          </tbody>
                          <tfoot>
                          <tr>
                            <th width="15%">Name</th>
                            <th width="10%">Email</th>
                            <th width="10%">Phone</th>
                            <th width="10%">Arrival</th>
                            <th width="10%">Departure</th>
                            <th width="10%">Guest</th>
                            <th width="15%">Message</th>
                            <th width="5%">Edit</th>
                            <th width="5%">Delete</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
        
    </section>
</section>
<!--main content end-->