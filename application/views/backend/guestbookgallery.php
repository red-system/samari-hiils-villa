<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
          <div class="col-lg-12">
              <!--breadcrumbs start -->
              <ul class="breadcrumb">
                  <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend <?php echo $title;?></li>
              </ul>
              <!--breadcrumbs end -->
          </div>
        </div>
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                <header class="panel-heading">
                    <strong>Backend <?php echo $title;?></strong>
                  <span class="tools pull-right">
                      <a href="javascript:;" class="icon-chevron-down"></a> 
                  </span>
                </header>
                <div class="panel-body">
                    <ul class="grid cs-style-3">
                        <?php foreach ($images as $pic):?>
                          <li>
                              <figure >
                                  <img  src="<?php echo base_url();?>assets/images/guestbook/<?php echo $pic['picture_name'];?>" alt="" style="width: 351px; height: auto;">
                                  
                                  <figcaption>
                                      <a class="btn btn-danger btn-block" href="<?php echo base_url();?>backend_home/image_delete/<?php echo $pic['general_id'];?>" onclick="return confirm('Are you sure to delete this picture?')" type="button"><i class="icon-trash"></i> Delete image</a> 
                                  </figcaption>
                              </figure>
                          </li>
                        <?php endforeach;?>
                    </ul>
                    <a type="button" class="btn btn-round btn-default btn-block" href="<?php echo base_url();?>backend_home/<?php echo $general_name;?>_image_add"><i class="icon-plus-sign"> </i> Add Picture </a>
                </div>
            </section>
            </div>
        </div>
        <!-- page end-->
       
    </section>
</section>
<!--main content end-->