<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li><a href="<?php echo base_url();?>backend_availability/<?php echo $general_name;?>"> <?php echo $title; ?></a></li>
					<li class="active"> Edit <?php echo $title; ?></li>
				</ul>
				<!--breadcrumbs end -->
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
                      	Form Edit <?php echo $title; ?>
                      	<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
                  	</header>
					<div class="panel-body">
						<?php echo form_open_multipart('backend_availability/reservation_editprocess','class="form-horizontal tasi-form"','method="post"'); ?>
                    		
                    		<div class="form-group">
								<label class="col-sm-2 ">Name <font color=red>*</font></label>
								<div class="col-sm-10">
									<input type="hidden" class="form-control" placeholder="Enter Name" name="id_reservation" value="<?php echo $reservation['id_reservasi'];?>" >
									<input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $reservation['name'];?>" required="" >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 ">Email <font color=red>*</font></label>
								<div class="col-sm-10">
									<input type="email" class="form-control" placeholder="Enter Email" name="email" required="" value="<?php echo $reservation['email'];?>">
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 ">Phone Number</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" placeholder="Enter Phone Number" name="phone" value="<?php echo $reservation['phone'];?>">
								</div>
							</div>

							<div class="form-group">
	                            <label class="col-sm-2 ">Arrival <font color=red>*</font></label>
	                            <div class="col-sm-4">
	                            	<?php
	                                	$year =  substr ($reservation['start'],0,4);
									    $month = substr ($reservation['start'],5,2);
									    $date =  substr ($reservation['start'],8,2);
									    $start = $month."-".$date."-".$year;
	                                ?>
	                                <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="<?php echo $start;?>" name="arrival" required=""/>
                                      <span class="help-block">Select date</span>
	                            </div>
	                            <label class="col-sm-2 ">Departure <font color=red>*</font></label>
	                            <div class="col-sm-4">
	                            	<?php
	                                	$year =  substr ($reservation['end'],0,4);
									    $month = substr ($reservation['end'],5,2);
									    $date =  substr ($reservation['end'],8,2);
									    $end = $month."-".$date."-".$year;
	                                ?>
	                                <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="<?php echo $end;?>" name="departure" required=""/>
                                      <span class="help-block">Select date</span>
	                            </div>
	                        </div>

	                        <div class="form-group">
								<label class="col-sm-2 ">Guest <font color=red>*</font></label>
								<div class="col-sm-10">
									<select class="form-control m-bot15" name="guest" required="">
										<option value="1-2" <?php if($reservation['guest']=='1-2'){echo('selected');} ?>>1-2</option>
										<option value="3-4" <?php if($reservation['guest']=='3-4'){echo('selected');} ?>>3-4</option>
										<option value="5-10" <?php if($reservation['guest']=='5-10'){echo('selected');} ?>>5-10</option>
                                    </select>
								</div>
							</div>

							<div class="form-group">
				                <label class="col-sm-2">Message </label>
				                <div class="col-lg-10">
				                  <textarea class="form-control" name="message" rows="7"><?php echo $reservation['message'];?></textarea><br>
				                </div>
				            </div>
							
                          	
							
							<div class="form-group">
								<div class="col-lg-12">
									<a class="btn btn-shadow btn-white" title="back" href="<?php echo base_url();?>backend_availability/<?php echo $general_name;?>" type="button"><i class="icon-reply"></i> Back</a>
									<button class="btn btn-shadow btn-success pull-right" type="submit" >Save  <i class=" icon-ok"></i></button>
								</div>
							</div>
						</form>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<!--main content start-->



             