<section id="main-content">
	<section class="wrapper">
		<div class="row">
	        <div class="col-lg-12">
	            <!--breadcrumbs start -->
	            <ul class="breadcrumb">
	                <li><a href="<?php echo base_url();?>backend/"><i class="icon-dashboard"></i> Dashboard</a></li>
	                <li class="active">Backend Menu &amp; Title</li>
	            </ul>
	            <!--breadcrumbs end -->
	        </div>
	      </div>
	    <div class="row">
	        <div class="col-lg-12">
	            <section class="panel">
					<header class="panel-heading">
						Form Backend Menu &amp; Title
						<span class="tools pull-right">
                        	<a href="javascript:;" class="icon-chevron-down"></a>	
                      	</span>
					</header>
					<div class="panel-body">
						<?php 
						   if($this->session->flashdata('true')){
						 ?>
						   <div class="alert alert-success"> 
						     <?php  echo $this->session->flashdata('true'); ?>
						    </div>
						<?php    
						}else if($this->session->flashdata('err')){
						?>
						 <div class = "alert alert-success">
						   <?php echo $this->session->flashdata('err'); ?>
						 </div>
						<?php } ?>
						<?php echo form_open_multipart('backend_menu_title/update_menutitle','class="form-horizontal tasi-form"'); ?>
							<div class="form-group">
								<label class="col-sm-2">Template Name</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="viewdetailen" value="<?php echo $viewdetailen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="viewdetailde" value="<?php echo $viewdetailde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Template Name</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="teammemberen" value="<?php echo $teammemberen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="teammemberde" value="<?php echo $teammemberde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Template Name</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="groundplanen" value="<?php echo $groundplanen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="groundplande" value="<?php echo $groundplande['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 1</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="homeen" value="<?php echo $homeen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="homede" value="<?php echo $homede['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 2</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="thevillaen" value="<?php echo $thevillaen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="thevillade" value="<?php echo $thevillade['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 3</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="locationen" value="<?php echo $locationen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="locationde" value="<?php echo $locationde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 4</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="offersen" value="<?php echo $offersen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="offersde" value="<?php echo $offersde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 5</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="galleryen" value="<?php echo $galleryen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="galleryde" value="<?php echo $galleryde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 6</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="availabilityen" value="<?php echo $availabilityen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="availabilityde" value="<?php echo $availabilityde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 7</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="guestbooken" value="<?php echo $guestbooken['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="guestbookde" value="<?php echo $guestbookde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 8</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="pricingen" value="<?php echo $pricingen['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="pricingde" value="<?php echo $pricingde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2">Menu 9</label>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">English</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="contacten" value="<?php echo $contacten['title'];?>" />
								</div>
								<div class="col-sm-5">
									<label class="col-sm-2 pull">German</label>
									<input type="text" class="form-control" placeholder="Enter Name" name="contactde" value="<?php echo $contactde['title'];?>" />
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12">
									<button class="btn btn-shadow btn-primary pull-right" type="submit" name="action">Update
									<i class=" icon-repeat"></i></button>
								</div>
							</div>
	                      
						</form>
					</div>
				</section>

	        </div>
	    </div>
	</section>
</section>