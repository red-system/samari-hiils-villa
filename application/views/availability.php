<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<section class="section-about">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div id="calendar"></div>

        </div>
      </div>
    </div>
</section>


<script type="text/javascript">
    $(function() {
        <?php 
              $array= array();
              $status = '';

        ?>    

        <?php $i=0;
         foreach ($days as $key => $value) { 
            
            $year =  substr ($value['day'],0,4);
            $month =  substr ($value['day'],5,2);
            $mm = $month-1;
            $date =  substr ($value['day'],8,2);

            $count = $this->db->get_where('tb_day', array('day' => $value['day']) )->num_rows();
            
            if ($count > 1 ) { ?>
                var InOut<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                <?php $status = 'InOut'.$i; ?>
            <?php } else {
                
                $check = $this->db->get_where('tb_day', array('day' => $value['day']))->row_array();
                if ($check['info']=='checkIn') { ?>
                    var checkIn<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                    <?php $status = 'checkIn'.$i ?>                        

                <?php } else {
                    if ($check['info']=='checkOut') { ?>
                        var checkOut<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                        <?php $status = 'checkOut'.$i ?>

                    <?php } else { 
                        if ($check['info']=='booked') { ?>
                           var booked<?php echo $i; ?> = new Date(<?php echo $year;?>, <?php echo $mm;?>, <?php echo $date;?>).getTime();
                           <?php $status = 'booked'.$i ?>
                        <?php } ?>
                    
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php 
            $array[$value['day']] = $status;
            $i++;

            }
        ?>


    $('#calendar').calendar({
        customDayRenderer: function(element, date) {

            <?php foreach($array as $tanggal=>$status) { ?>
            if(date.getTime() ==    <?php echo $status ?>) {

                <?php if(strpos($status, 'checkIn') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkIn.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>


                <?php if(strpos($status, 'checkOut') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkOut.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>

                <?php if(strpos($status, 'booked') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/booked.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>

                <?php if(strpos($status, 'InOut') !== false) { ?>
                    $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkInOut.svg)');
                    $(element).css('background-repeat', 'no-repeat');
                    $(element).css('background-size', '100% 100%');
                    $(element).css('background-position', '50% 50%');
                <?php } ?>

            }
            <?php } ?>

/*
            if(date.getTime() == checkIn) {
                $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkIn.svg)');
                $(element).css('background-repeat', 'no-repeat');
                $(element).css('background-size', '100% 100%');
                $(element).css('background-position', '50% 50%');
            } 
            if (date.getTime() == booked) {
                $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/booked.svg)');
                $(element).css('background-repeat', 'no-repeat');
                $(element).css('background-size', '100% 100%');
                $(element).css('background-position', '50% 50%');
            } 
            if (date.getTime() == checkOut) {
                $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkOut.svg)');
                $(element).css('background-repeat', 'no-repeat');
                $(element).css('background-size', '100% 100%');
                $(element).css('background-position', '50% 50%');
            } 
            if (date.getTime() == checkInOut) {
                $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/checkInOut.svg)');
                $(element).css('background-repeat', 'no-repeat');
                $(element).css('background-size', '100% 100%');
                $(element).css('background-position', '50% 50%');
            }
            if (date.getTime() == booked1) {
                $(element).css('background-image', 'linear-gradient(transparent,transparent),url(<?php echo base_url();?>assets/images/booked.svg)');
                $(element).css('background-repeat', 'no-repeat');
                $(element).css('background-size', '100% 100%');
                $(element).css('background-position', '50% 50%');
            }*/
            
        }
    });
});
</script>