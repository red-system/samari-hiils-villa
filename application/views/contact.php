<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- RESERVATION -->
<section class="section-reservation-page bg-white">

    <div class="container">
        <div class="reservation-page">
            
            <div class="row">
                
                <div class="col-md-12 ">

                    <div class="reservation-sidebar">

                        <!-- SIDEBAR AVAILBBILITY -->
                        <div class="reservation-sidebar_availability bg-gray">

                            <!-- HEADING -->
                            <h2 class="reservation-heading"><?php echo $reservation['general_data'];?></h2>
                            <!-- END / HEADING -->
                            <?php echo form_open_multipart($lang.'/contact/send-reservation','class="form-horizontal tasi-form"'); ?>
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_sub_data'];?> <font color=red>*</font></label>
                                    <input style="width: 100%;border-width:0;height: 30px;" type="text" class="awe-input from" placeholder="<?php echo $reservation['general_sub_data'];?>" name="name" required>
                                </div>

                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_desc'];?> <font color=red>*</font></label>
                                    <input style="width: 100%;border-width:0;height: 30px;" type="email" class="awe-input from" placeholder="<?php echo $reservation['general_desc'];?>" name="email" required>
                                </div>

                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_sub_desc'];?></label>
                                    <input style="width: 100%;border-width:0;height: 30px;" type="text" class="awe-input from" placeholder="<?php echo $reservation['general_sub_desc'];?>" name="phone">
                                </div>

                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['general_link'];?></label>
                                    <input type="text" class="awe-calendar awe-input from" placeholder="<?php echo $reservation['general_link'];?>" name="arrival">
                                </div>
                                
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['main_image'];?></label>
                                    <input type="text" class="awe-calendar awe-input to" placeholder="<?php echo $reservation['main_image'];?>" name="departure">
                                </div>
                                
                                <div class="check_availability-field col-md-4">
                                    <label><?php echo $reservation['secondary_image'];?></label>
                                    <select class="awe-select" name="guest">
                                        <option value="0"><?php echo $reservation['secondary_image'];?></option>
                                        <option value="1-2">1-2</option>
                                        <option value="3-4">3-4</option>
                                        <option value="5-10">5-10</option>
                                    </select>
                                </div>

                                <div class="check_availability-field col-md-12">
                                    <label><?php echo $reservation['general_password'];?> <font color=red>*</font></label>
                                    <textarea style="width: 100%;border-width:0;" name="message" class="awe-input from" placeholder="<?php echo $reservation['general_password'];?>" required></textarea>
                                </div>
                                <div class="check_availability-field col-md-3">
                                    <?php echo $captcha_img;?>
                                </div>
                                <div class="col-md-3">
                                    <br>
                                    <input type="text" class="form-control" name="captcha" placeholder="<?php if( $lang == 'de') { echo 'Captcha eingeben'; } else  if ( $lang == 'de' ) { echo 'Input captcha'; } ?>" required>
                                </div>
                                
                                <button class="awe-btn awe-btn-13" type="submit" ><?php echo $reservation['general_additional_info'];?></button>
                            </form>
                        </div>
                        <!-- END / SIDEBAR AVAILBBILITY -->

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- END / RESERVATION -->

<section class="section-contact" style="padding-top: 0px; padding-bottom: 40px;">
    <div class="container">
        <div class="contact">
            <div class="row">

                <div class="col-md-12">

                    <div class="text-center">
                        <h2><?php echo $content['general_data'];?></h2>
                    </div>

                    <div class="text" style="margin-top: 15px;">
                        <p><?php echo $content['general_desc'];?></p>
                        <ul style="margin-top: 0px;">
                            <li><i class="icon lotus-icon-location"></i> <?php echo $address['general_data'];?></li>
                            <li><i class="icon lotus-icon-phone"></i> <a style="color: black" href="tel:<?php echo $phone1['general_data'];?>"><?php echo $phone1['general_data'];?></a></li>
                            <li><i class="icon lotus-icon-phone"></i> <a style="color: black" href="tel:<?php echo $phone2['general_data'];?>"><?php echo $phone2['general_data'];?></a></li>
                            <li><i class="icon fa fa-envelope-o"></i> <a style="color: black" href="mailto:<?php echo $email['general_data'];?>" ><?php echo $email['general_data'];?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 15px;">
                    <div class="text">
                        <h3><strong> <?php echo $content['general_sub_data'];?></strong></h3>
                        <p><?php echo $content['general_sub_desc'];?></p>
                        
                    </div>
                </div>
            </div> 
        </div>  
    </div>
</section>
<!-- END / CONTACT -->

<!-- MAP -->
<section class="section-map">
    <h1 class="element-invisible">Map</h1>
    <div class="contact-map">
        <div id="tempmap" style="position: absolute; height: 100%; width: 100%;"></div>
        <?php
        foreach($data->result() as $row)
        {
          $nama_lokasi = $row->name;
          $lat = $row->latitude;
          $lng = $row->longitude;
        }
        ?>

        <!-- <script type="text/javascript" 
        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMe_D6gusKtAYXmSwgO8iXFPlb4c0iOwU&sensor=false">
        </script>-->
        <script type="text/javascript">
        (function() {
           window.onload = function(){
            var myLatlng = new google.maps.LatLng(<?php echo $lat.",".$lng; ?>);
            var mapOptions = {
            zoom: 15,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("tempmap"), mapOptions);
          
            var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              title:"<?php echo $nama_lokasi ?>"
            });
            
           }
        })();
        </script>
    </div>
</section>
<!-- END / MAP -->