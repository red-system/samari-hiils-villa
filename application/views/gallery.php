<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->

<!-- ABOUT -->
<section class="section-about">
    <div class="container">

        <div class="col-md-12">

            <!-- ITEM -->
            <div class="col-md-6">

                <div class="text">
                    <h3><strong> <?php echo $first['general_data'];?></strong></h3><br>
                    <div class="desc">
                        <p><?php echo $first['general_desc'];?></p>
                        
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="col-md-6">

                <div class="text">
                    <h3><strong> <?php echo $second['general_data'];?></strong></h3><br>
                    <div class="desc">
                        <p><?php echo $second['general_desc'];?></p>
                        
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            

        </div>

    </div>
</section>
<!-- END / ABOUT -->

<!-- GALLERY -->
<section class="section_page-gallery">
    <div class="container">
        <div class="gallery">

            <!-- FILTER -->
            <div class="gallery-cat text-center">
                <ul class="list-inline">
                    <li><a href="#" data-filter=".villa"><?php if( $lang == 'de') { echo $villa['general_sub_data']; } else  if ( $lang == 'en' ) { echo $villa['general_data']; } ?></a></li>
                    <li><a href="#" data-filter=".facets"><?php if( $lang == 'de') { echo $facets['general_sub_data']; } else  if ( $lang == 'en' ) { echo $facets['general_data']; } ?></a></a></li>
                    <li><a href="#" data-filter=".additional"><?php if( $lang == 'de') { echo $additional['general_sub_data']; } else  if ( $lang == 'en' ) { echo $additional['general_data']; } ?></a></li>
                </ul>
            </div>
            <!-- END / FILTER -->

            <!-- GALLERY CONTENT -->
            <div class="gallery-content">
                <div class="row">
                    <div class="gallery-isotope col-4">

                        <!-- ITEM SIZE -->
                        <div class="item-size "></div>
                        <!-- END / ITEM SIZE -->
                        <?php foreach ($imagethevilla as $picvilla):?>
                            <!-- ITEM -->
                            <div class="item-isotope  villa">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picvilla['picture_name'];?>" class="mfp-image" title="Samari Hill Villas">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picvilla['second_picture'];?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        <?php endforeach;?>

                        <?php foreach ($imagefacets as $picfacets):?>
                            <!-- ITEM -->
                            <div class="item-isotope  facets">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picfacets['picture_name'];?>" class="mfp-image" title="Samari Hill Villas">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picfacets['second_picture'];?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        <?php endforeach;?>

                        <?php foreach ($imageadditional as $picadditional):?>
                            <!-- ITEM -->
                             <div class="item-isotope  additional">
                                <div class="gallery_item">
                                    <a href="<?php echo base_url();?>assets/images/gallery/<?php echo $picadditional['picture_name'];?>" class="mfp-image" title="Samari Hill Villas">
                                        <img src="<?php echo base_url();?>assets/images/gallery/thumb/<?php echo $picadditional['second_picture'];?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- END / ITEM -->
                        <?php endforeach;?>
                        
                        
                        
                    </div>
                </div>

            </div>
            <!-- GALLERY CONTENT -->

        </div>
    </div>       
</section>
<!-- END / GALLERY -->

