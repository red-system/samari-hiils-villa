<!-- SUB BANNER -->
<section class="section-sub-banner bg-9" style="background: url(<?php echo base_url();?>assets/images/banner/<?php echo $imageheader['picture_name'];?>) no-repeat; background-attachment: scroll; background-size: 1920px;">
    <div class="awe-overlay"></div>
    <div class="sub-banner">
        <div class="container">
            <div class="text text-center">
                <h2><?php echo $header['general_data'];?></h2>
                <p><?php echo $header['general_desc'];?></p>
            </div>
        </div>

    </div>

</section>
<!-- END / SUB BANNER -->
<!-- ABOUT -->
<section class="section-about">
    <div class="container">

        <div class="about">

            <!-- ITEM -->
            <div class="about-item">

                <div class="img owl-single">
                    <?php foreach ($imageabout as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>

                    
                </div>

                <div class="text" style="margin-top: 30px;">
                    <h3><strong> <?php echo $about['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $about['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="about-item about-right">

                <div class="img owl-single">
                    <?php foreach ($imagecondition as $picabout):?>
                    <div class="gallery_item">
                        <a href="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" class="mfp-image">
                             <img src="<?php echo base_url();?>assets/images/location/<?php echo $picabout['picture_name'];?>" alt="">
                        </a>
                    </div>
                    <?php endforeach;?>
                </div>

                <div class="text">
                    <h3><strong> <?php echo $condition['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $condition['general_desc'];?></p>
                    </div>
                </div>

            </div>
            <!-- END / ITEM -->

            <!-- ITEM -->
            <div class="col-md-12" style="margin-top: 60px">
                <div class="text-center">
                    <h3><strong> <?php echo $map['general_data'];?></strong></h3>
                    <div class="desc">
                        <p><?php echo $map['general_desc'];?></p>
                    </div><br>
                </div>
                <div class="contact-map">
                    <div id="tempmap" style="position: absolute; height: 100%; width: 100%;"></div>
                    <?php
                    foreach($data->result() as $row)
                    {
                      $nama_lokasi = $row->name;
                      $lat = $row->latitude;
                      $lng = $row->longitude;
                    }
                    ?>

                    <!-- <script type="text/javascript" 
                    src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBMe_D6gusKtAYXmSwgO8iXFPlb4c0iOwU&sensor=false">
                    </script>-->
                    <script type="text/javascript">
                    (function() {
                       window.onload = function(){
                        var myLatlng = new google.maps.LatLng(<?php echo $lat.",".$lng; ?>);
                        var mapOptions = {
                        zoom: 15,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                        }
                        var map = new google.maps.Map(document.getElementById("tempmap"), mapOptions);
                      
                        var marker = new google.maps.Marker({
                          position: myLatlng,
                          map: map,
                          title:"<?php echo $nama_lokasi ?>"
                        });
                        
                       }
                    })();
                    </script>
                </div>
            </div>
            <!-- END / ITEM -->

        </div>

    </div>
</section>
<!-- END / ABOUT -->

