<?php
class Backend_thevillamodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_thevillaall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function getrow_thevillaall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_thevillafront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function get_thevilla_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_thevillaimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_thevillaimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function update_thevilla(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_headerthevilla($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','header_villa');
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  public function team_member_add($add_data){
    $image = $add_data['file_name'];
    $data = array(
        'general_name' => 'team_member',
        'general_data' => $this->input->post('name'),
        'main_image' => $image,
        'general_desc' => $this->input->post('positionen'),
        'general_sub_desc' => $this->input->post('positionde'),
        'email_link' => $this->input->post('email'),
        'facebook_link' => $this->input->post('facebook'),
        'instagram_link' => $this->input->post('instagram'),
        'twitter_link' => $this->input->post('twitter')
    );
    return $this->db->insert('tb_general_data',$data);
  }

  public function update_team_member($add_data){
    $general_id = $this->input->post('general_id');
    $image = $add_data['file_name'];
    if(isset($image)){
      $data = array(
        'general_data' => $this->input->post('name'),
        'main_image' => $image,
        'general_desc' => $this->input->post('positionen'),
        'general_sub_desc' => $this->input->post('positionde'),
        'email_link' => $this->input->post('email'),
        'facebook_link' => $this->input->post('facebook'),
        'instagram_link' => $this->input->post('instagram'),
        'twitter_link' => $this->input->post('twitter')
      );
    } else {
      $data = array(
        'general_data' => $this->input->post('name'),
        'general_desc' => $this->input->post('positionen'),
        'general_sub_desc' => $this->input->post('positionde'),
        'email_link' => $this->input->post('email'),
        'facebook_link' => $this->input->post('facebook'),
        'instagram_link' => $this->input->post('instagram'),
        'twitter_link' => $this->input->post('twitter')
      );
    }
    $this->db->where('general_id',  $general_id);
    return $this->db->update('tb_general_data', $data);
  }

  public function delete_team_member($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_general_data');
    return true;
  }
}