<?php
class Backend_menutitlemodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_menutitle($slug){
    $query = $this->db->get_where('tb_kamus',array('param' => $slug));
    return $query->row_array();
  }

  public function getrow_menutitlefront($title,$slug){
    $query = $this->db->get_where('tb_kamus', array('kategori' => $title,'id_lang' => $slug));
    return $query->row_array();
  }

  public function update_viewdetailen(){
    $data = array(
      'title' => $this->input->post('viewdetailen')
    );
    $this->db->where('param','viewdetailen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_viewdetailde(){
    $data = array(
      'title' => $this->input->post('viewdetailde')
    );
    $this->db->where('param','viewdetailde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_homeen(){
    $data = array(
      'title' => $this->input->post('homeen')
    );
    $this->db->where('param','homeen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_homede(){
    $data = array(
      'title' => $this->input->post('homede')
    );
    $this->db->where('param','homede');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_thevillaen(){
    $data = array(
      'title' => $this->input->post('thevillaen')
    );
    $this->db->where('param','thevillaen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_thevillade(){
    $data = array(
      'title' => $this->input->post('thevillade')
    );
    $this->db->where('param','thevillade');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_locationen(){
    $data = array(
      'title' => $this->input->post('locationen')
    );
    $this->db->where('param','locationen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_locationde(){
    $data = array(
      'title' => $this->input->post('locationde')
    );
    $this->db->where('param','locationde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_offersen(){
    $data = array(
      'title' => $this->input->post('offersen')
    );
    $this->db->where('param','offersen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_offersde(){
    $data = array(
      'title' => $this->input->post('offersde')
    );
    $this->db->where('param','offersde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_galleryen(){
    $data = array(
      'title' => $this->input->post('galleryen')
    );
    $this->db->where('param','galleryen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_galleryde(){
    $data = array(
      'title' => $this->input->post('galleryde')
    );
    $this->db->where('param','galleryde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_availabilityen(){
    $data = array(
      'title' => $this->input->post('availabilityen')
    );
    $this->db->where('param','availabilityen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_availabilityde(){
    $data = array(
      'title' => $this->input->post('availabilityde')
    );
    $this->db->where('param','availabilityde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_guestbooken(){
    $data = array(
      'title' => $this->input->post('guestbooken')
    );
    $this->db->where('param','guestbooken');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_guestbookde(){
    $data = array(
      'title' => $this->input->post('guestbookde')
    );
    $this->db->where('param','guestbookde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_pricingen(){
    $data = array(
      'title' => $this->input->post('pricingen')
    );
    $this->db->where('param','pricingen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_pricingde(){
    $data = array(
      'title' => $this->input->post('pricingde')
    );
    $this->db->where('param','pricingde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_contacten(){
    $data = array(
      'title' => $this->input->post('contacten')
    );
    $this->db->where('param','contacten');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_contactde(){
    $data = array(
      'title' => $this->input->post('contactde')
    );
    $this->db->where('param','contactde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_teammemberen(){
    $data = array(
      'title' => $this->input->post('teammemberen')
    );
    $this->db->where('param','teammemberen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_teammemberde(){
    $data = array(
      'title' => $this->input->post('teammemberde')
    );
    $this->db->where('param','teammemberde');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_groundplanen(){
    $data = array(
      'title' => $this->input->post('groundplanen')
    );
    $this->db->where('param','groundplanen');
    return $this->db->update('tb_kamus',$data);
  }

  public function update_groundplande(){
    $data = array(
      'title' => $this->input->post('groundplande')
    );
    $this->db->where('param','groundplande');
    return $this->db->update('tb_kamus',$data);
  }
  



}