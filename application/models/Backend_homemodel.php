<?php
class Backend_homemodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function getrow_homefront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_homerow($slug){
    $query = $this->db->get_where('tb_general_data',array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function get_homefeaturerow($id,$slug){
    $query = $this->db->get_where('tb_general_data',array('general_ref_id' => $id,$slug, 'general_sub_name' => $slug));
    return $query->row_array();
  }

  public function get_homeall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->result_array();
  }

  public function get_homearrayen($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug,'general_lang' => 'en'));
    return $query->result_array();
  }

  public function get_homearrayde($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug,'general_lang' => 'de'));
    return $query->result_array();
  }

  public function get_homerowen($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug,'general_lang' => 'en'));
    return $query->row_array();
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function get_homeimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_homeimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function get_homerowde($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug,'general_lang' => 'de'));
    return $query->row_array();
  }

  public function get_home_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_homearray_by_ref_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_ref_id' => $id));
      return $query->result_array();
  }

  public function update_picture($additional_data){
    $data  = array(
       'main_image'      => $additional_data['file_name']
    );
   
    $this->db->where('general_sub_name','slide');
    return $this->db->update('tb_general_data',$data);
  }

  

  public function update_titleen(){
    $data = array(
      'general_data' => $this->input->post('line1en'),
      'general_sub_data' => $this->input->post('line2en')
    );
    $this->db->where('general_id', $this->input->post('general_iden'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_titlede(){
    $data = array(
      'general_data' => $this->input->post('line1de'),
      'general_sub_data' => $this->input->post('line2de')
    );
    $this->db->where('general_id', $this->input->post('general_idde'));
    return $this->db->update('tb_general_data',$data);
  }

  public function get_menulink(){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => 'menulink'));
    return $query->result_array();
  }

  public function update_menulink(){
    $data = array(
      'general_data' => $this->input->post('name')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_menulinkimage($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_id',$this->input->post('id'));
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  public function update_aboutus(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_ourbest(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature1(){
    $data = array(
      'general_data' => $this->input->post('feature1')
    );
    $this->db->where('general_id', $this->input->post('id1'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature2(){
    $data = array(
      'general_data' => $this->input->post('feature2')
    );
    $this->db->where('general_id', $this->input->post('id2'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature3(){
    $data = array(
      'general_data' => $this->input->post('feature3')
    );
    $this->db->where('general_id', $this->input->post('id3'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature4(){
    $data = array(
      'general_data' => $this->input->post('feature4')
    );
    $this->db->where('general_id', $this->input->post('id4'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature5(){
    $data = array(
      'general_data' => $this->input->post('feature5')
    );
    $this->db->where('general_id', $this->input->post('id5'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_feature6(){
    $data = array(
      'general_data' => $this->input->post('feature6')
    );
    $this->db->where('general_id', $this->input->post('id6'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_featureicon($slug, $additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id',$slug);
    return $this->db->update('tb_picture',$data);
  }

  public function update_feature($data){
    $this->db->where('general_id', $data['general_id']);
    $this->db->update('tb_general_data',$data);
    return;
  }

  

  

}