<?php
class Backend_pricingmodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_pricingall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function getrow_pricingall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_pricingfront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function getrow_map(){
    $query = $this->db->get_where('tb_pricing', array('id_pricing' => '1'));
    return $query->row_array();
  }

  public function get_pricing_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_pricingimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_pricingimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function update_pricing(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_pricelist($additional_data = FALSE){
    if(isset($additional_data['file_name'])){
      $data = array(
      'general_data' => $this->input->post('titleen'),
      'general_sub_data' => $this->input->post('titlede'),
      'main_image'   => $additional_data['file_name']
      );
    } else {
      $data = array(
      'general_data' => $this->input->post('titleen'),
      'general_sub_data' => $this->input->post('titlede')     
      );
    }
    $this->db->where('general_sub_name','pricinglist');
    return $this->db->update('tb_general_data',$data);
  }

  public function update_headerpricing($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id','header_pricing');
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  
}