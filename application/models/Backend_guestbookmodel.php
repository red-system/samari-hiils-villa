<?php
class Backend_guestbookmodel extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function get_guestbookall($slug){
    $this->db->order_by('general_created_at', 'desc');
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug));
    return $query->result_array();
  }

  public function get_guestbookhome($slug){
    $this->db->order_by('rand()');
    $this->db->limit(5, 0);
    $query = $this->db->get_where('tb_general_data', array('general_name' => $slug , 'general_additional_info' => 'checked'));
    return $query->result_array();
  }

  public function getrow_guestbookall($slug){
    $query = $this->db->get_where('tb_general_data', array('general_sub_name' => $slug));
    return $query->row_array();
  }

  public function getrow_guestbookfront($title,$slug){
    $query = $this->db->get_where('tb_general_data', array('general_name' => $title,'general_lang' => $slug));
    return $query->row_array();
  }

  public function record_count() {
    $query = $this->db->get_where('tb_general_data', array('general_name' => 'guestbooklist' , 'general_additional_info' => 'checked'));
    return $query->num_rows();
  }

  public function get_guestbook_page($limit, $start){
    $this->db->order_by('general_updated_at', 'DESC');
    $query = $this->db->where(array('general_name' => 'guestbooklist' , 'general_additional_info' => 'checked'))->get('tb_general_data',$limit, $start);
    return $query;
  }

  public function get_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->result_array();
  }

  public function getrow_image($slug){
    $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
    return $query->row_array();
  }

  public function get_guestbook_by_id($id){
      $query = $this->db->get_where('tb_general_data', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_guestbookimage_by_id($id){
      $query = $this->db->get_where('tb_picture', array('general_id' => $id));
      return $query->row_array();
  }

  public function get_guestbookimage_by_refid($slug){
      $query = $this->db->get_where('tb_picture', array('general_ref_id' => $slug));
      return $query->row_array();
  }

  public function update_guestbook(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description'),
      'general_sub_desc' => $this->input->post('button')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_formguestbook(){
    $data = array(
      'general_data' => $this->input->post('name'),
      'general_desc' => $this->input->post('description'),
      'general_sub_data' => $this->input->post('line1'),
      'general_sub_desc' => $this->input->post('line2'),
      'general_link' => $this->input->post('line3'),
      'main_image' => $this->input->post('line4'),
      'secondary_image' => $this->input->post('line5'),
      'general_password' => $this->input->post('button')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_guestbooklist(){
    $data = array(
      'general_additional_info' => $this->input->post('published')
    );
    $this->db->where('general_id', $this->input->post('general_id'));
    return $this->db->update('tb_general_data',$data);
  }

  public function update_headerguestbook($additional_data){
    $data  = array(
       'picture_name'      => $additional_data['file_name']
    );
   
    $this->db->where('general_ref_id', $additional_data['ref_id']);
    return $this->db->update('tb_picture',$data);
  }

  public function image_add($data){
    $this->db->insert('tb_picture', $data);
    return;
  }

  public function image_delete($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_picture');
    return true;
  }

  public function guestbook_add($add_data){
    $image = $add_data['file_name'];
    $data = array(
        'general_name' => 'guestbooklist',
        'general_data' => $this->input->post('name'),
        'main_image' => $image,
        'general_desc' => $this->input->post('title'),
        'general_sub_desc' => $this->input->post('testimonial'),
        'general_link' => $this->input->post('location'),
        'general_additional_info' => ''
    );
    return $this->db->insert('tb_general_data',$data);
  }

  public function delete_guestbook_list($id){
    $this->db->where('general_id', $id);
    $this->db->delete('tb_general_data');
    return true;
  }

  
}