<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest_book extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
    function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}

	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'guest_book');

		//def
		$data['lang'] = $this->lang->lang();
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookheader',$data['lang']);
		$data['about'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookabout',$data['lang']);
		$data['modal'] = $this->backend_guestbookmodel->getrow_guestbookfront('guestbookform',$data['lang']);
		$data['imageheader'] = $this->backend_guestbookmodel->getrow_image('header_guestbook');

		//konfigurasi pagination
        $config['base_url'] = site_url($data['lang'].'/guest-book/index'); //site url
        $config['total_rows'] = $this->backend_guestbookmodel->record_count();//total row
        $config['per_page'] = 12;  //show record per halaman
        $config["num_links"] = 5;
 		
      	$config['full_tag_open']   = '<ul class="page-navigation text-center">';       
      	$config['full_tag_close']  = '</ul>';                
      	$config['first_link']      = '<b>First</b>';         
      	$config['first_tag_open']  = '<li>';       
      	$config['first_tag_close'] = '</li>';                
      	$config['last_link']       = '<b>Last</b>';         
      	$config['last_tag_open']   = '<li>';        
      	$config['last_tag_close']  = '</li>';                
      	$config['next_link']       = ' <i class="fa fa-arrow-right"></i> ';         
      	$config['next_tag_open']   = '<li>';        
      	$config['next_tag_close']  = '</li>';                
      	$config['prev_link']       = ' <i class="fa fa-arrow-left"></i> ';         
      	$config['prev_tag_open']   = '<li>';        
      	$config['prev_tag_close']  = '</li>';                
      	$config['cur_tag_open']    = '<li class="current-page"><a href="#">';        
      	$config['cur_tag_close']   = '</a></li>';                 
      	$config['num_tag_open']    = '<li>';        
      	$config['num_tag_close']   = '</li>';

		$this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
 		$data['data'] = $this->backend_guestbookmodel->get_guestbook_page($config["per_page"], $data['page']);   
        $data['pagination'] = $this->pagination->create_links();
        
 
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('guest_book');
		$this->load->view('templates/footer');
	}

	
	public function guestbook_addprocess()
	{
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$title = $this->input->post('title');
		$testimonial = $this->input->post('testimonial');
		$location = $this->input->post('location');

		$config['upload_path']          = './assets/images/guestbook/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('picture')){
			$data['file_name']= "noimage.png";
			$res=$this->backend_guestbookmodel->guestbook_add($data);
			if($res==true)
			{
			  $config = [
			       'useragent' => 'CodeIgniter',
			       'protocol'  => 'smtp',
			       'mailpath'  => '/usr/sbin/sendmail',
			       'smtp_host' => 'ssl://smtp.gmail.com',
			       'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
			       'smtp_pass' => 'agusarim',             // Password gmail Anda.
			       'smtp_port' => 465,
			       'smtp_keepalive' => TRUE,
			       'smtp_crypto' => 'SSL',
			       'wordwrap'  => TRUE,
			       'wrapchars' => 80,
			       'mailtype'  => 'html',
			       'charset'   => 'utf-8',
			       'validate'  => TRUE,
			       'crlf'      => "\r\n",
			       'newline'   => "\r\n",
			   	];

			   	// Load library email dan konfigurasinya.
				$this->email->initialize($config);

				// Pengirim dan penerima email.
				$this->email->from('info@samarihillvilla.de','Samari Hill Villas');    // Email dan nama pegirim.
				$this->email->to('agusdownload5293@gmail.com');   // Penerima email.

				// Lampiran email. Isi dengan url/path file.
				// $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');

				// Subject email.
				$this->email->subject('Information from guests');

				// Isi email. Bisa dengan format html.
				$this->email->message('
					<p>You have new testimonials from : </p>
					<p>Name  : <br>&emsp;<strong>'.$name. '</strong></p>
					<p>Email : <br>&emsp;<strong>'.$email. '</strong></p>
					<p>Location : <br>&emsp;<strong>'.$location. '</strong></p>
					<p>Testimonial Title : <br>&emsp;<strong>'.$title. '</strong></p>
					<p>Testimonial: <br>&emsp;<strong>'.$testimonial. '</strong></p>
		        	<br><br>
					<p>------------------------------------------------------------------------------------------------------------------------------------------</p>
					<p><strong>Sama Hill Villas              </strong> |  www.samarihillvillas.com  | </p>
					

					<p>------------------------------------------------------------------------------------------------------------------------------------------</p>'
				);

				$this->email->send();
				$this->session->set_flashdata('true', 
			  	'<h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Your testimony has been notified to the admin, and will be processed immediately.
				 </p>');
			}
			else
			{
			  echo "<script>alert('Message failed to send!');history.go(-1);</script>";
			}
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/guestbook/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 201;
            $config1['height']= 201;
            $config1['new_image']= './assets/images/guestbook/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data['file_name']= $gbr['file_name'];
           	$res=$this->backend_guestbookmodel->guestbook_add($data);
			if($res==true)
			{
			  $config = [
			       'useragent' => 'CodeIgniter',
			       'protocol'  => 'smtp',
			       'mailpath'  => '/usr/sbin/sendmail',
			       'smtp_host' => 'ssl://smtp.gmail.com',
			       'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
			       'smtp_pass' => 'agusarim',             // Password gmail Anda.
			       'smtp_port' => 465,
			       'smtp_keepalive' => TRUE,
			       'smtp_crypto' => 'SSL',
			       'wordwrap'  => TRUE,
			       'wrapchars' => 80,
			       'mailtype'  => 'html',
			       'charset'   => 'utf-8',
			       'validate'  => TRUE,
			       'crlf'      => "\r\n",
			       'newline'   => "\r\n",
			   	];

			   	// Load library email dan konfigurasinya.
				$this->email->initialize($config);

				// Pengirim dan penerima email.
				$this->email->from('info@samarihillvilla.de','Samari Hill Villas');    // Email dan nama pegirim.
				$this->email->to('agusdownload5293@gmail.com');   // Penerima email.

				// Lampiran email. Isi dengan url/path file.
				// $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');

				// Subject email.
				$this->email->subject('Information from guests');

				// Isi email. Bisa dengan format html.
				$this->email->message('
					<p>You have new testimonials from : </p>
					<p>Name  : <br>&emsp;<strong>'.$name. '</strong></p>
					<p>Email : <br>&emsp;<strong>'.$email. '</strong></p>
					<p>Location : <br>&emsp;<strong>'.$location. '</strong></p>
					<p>Testimonial Title : <br>&emsp;<strong>'.$title. '</strong></p>
					<p>Testimonial: <br>&emsp;<strong>'.$testimonial. '</strong></p>
		        	<br><br><br><br><br>
					<p>------------------------------------------------------------------------------------------------------------------------------------------</p>
					<p><strong>Sama Hill Villas              </strong> |  www.samarihillvillas.com  | </p>
					

					<p>------------------------------------------------------------------------------------------------------------------------------------------</p>'
				);

				$this->email->send();
				$this->session->set_flashdata('true', 
			  	'<h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Your testimony has been notified to the admin, and will be processed immediately.
				 </p>');
				}
			else
			{
			  echo "<script>alert('Message failed to send!');history.go(-1);</script>";
			}
			
		}

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	

}
