<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
    function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}

	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'gallery');

		//def
		$data['lang'] = $this->lang->lang();
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_gallerymodel->getrow_galleryfront('galleryheader',$data['lang']);
		$data['imageheader'] = $this->backend_gallerymodel->getrow_image('header_gallery');
		$data['first'] = $this->backend_gallerymodel->getrow_galleryfront('galleryfirst',$data['lang']);
		$data['second'] = $this->backend_gallerymodel->getrow_galleryfront('gallerysecond',$data['lang']);
		$data['villa'] = $this->backend_gallerymodel->getrow_galleryall('gallerythevilla');
		$data['imagethevilla'] = $this->backend_gallerymodel->get_image('gallery_thevilla');
		$data['facets'] = $this->backend_gallerymodel->getrow_galleryall('galleryfacets');
		$data['imagefacets'] = $this->backend_gallerymodel->get_image('gallery_facets');
		$data['additional'] = $this->backend_gallerymodel->getrow_galleryall('galleryadditional');
		$data['imageadditional'] = $this->backend_gallerymodel->get_image('gallery_additional');
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('gallery');
		$this->load->view('templates/footer');
	}

	

}
