<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
		$this->load->library('email');
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'contact');

		//def
		$data['lang'] = $this->lang->lang();
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_contactmodel->getrow_contactfront('contactheader',$data['lang']);
		$data['imageheader'] = $this->backend_contactmodel->getrow_image('header_contact');
		$data['reservation'] = $this->backend_contactmodel->getrow_contactfront('reservation',$data['lang']);
		$data['content'] = $this->backend_contactmodel->getrow_contactfront('contactcontent',$data['lang']);
		$data['data'] = $this->db->query("Select * from tb_location where id_location='1'");

		$options = array(
			'img_path' => './captcha/',
			'img_url' => base_url('captcha'),
			'img_width'=>'282', 
    	    'img_height'=>'70', 
   	      	'expiration'=> 7200,
   	      	'pool' => '0123456789',
   	      	'word_length'   => '7' );

		$cap = create_captcha($options);
		$this->session->set_userdata('keycode',md5($cap['word']));
   		$data['captcha_img'] = $cap['image'];
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('contact');
		$this->load->view('templates/footer');
	}

	function send_reservation() 
  	{
        $captcha = $this->input->post('captcha');
        if(md5($captcha)==$this->session->userdata('keycode'))
        {

	        // Konfigurasi email.
	        $config = [
	           'useragent' => 'CodeIgniter',
	           'protocol'  => 'smtp',
	           'mailpath'  => '/usr/sbin/sendmail',
	           'smtp_host' => 'ssl://smtp.gmail.com',
	           'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
	           'smtp_pass' => 'agusarim',             // Password gmail Anda.
	           'smtp_port' => 465,
	           'smtp_keepalive' => TRUE,
	           'smtp_crypto' => 'SSL',
	           'wordwrap'  => TRUE,
	           'wrapchars' => 80,
	           'mailtype'  => 'html',
	           'charset'   => 'utf-8',
	           'validate'  => TRUE,
	           'crlf'      => "\r\n",
	           'newline'   => "\r\n",
	        ];

	        // Load library email dan konfigurasinya.
	        $this->email->initialize($config);
	 
	        // Pengirim dan penerima email.
	        $this->email->from($this->input->post('email'),$this->input->post('name'));    // Email dan nama pegirim.
	        $this->email->to('agusdownload5293@gmail.com');                       // Penerima email.
	 
	        // Lampiran email. Isi dengan url/path file.
	        // $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');
	 
	        // Subject email.
	        $this->email->subject('Reservation from info@samarihillvillas.com');
	 
	        // Isi email. Bisa dengan format html.
	        $this->email->message(
	        	'Dear Reservation,<br /><br />
	        	I have browsing your website and and I would like<br />
				to booking your villas and<br />
				my reservation detail as bellow:<br /><br />
				--------------- RESERVATION DATA ---------------<br />
				Name : ' . $this->input->post('name') . ' <br>
				Email : ' . $this->input->post('email') . ' <br>
				Phone Number: ' . $this->input->post('phone') . '<br>
				Arrival: ' . $this->input->post('arrival') . '<br>
				Departure: ' . $this->input->post('departure') . '<br>
				Guest: ' . $this->input->post('guest') . '<br>
				Message: ' . $this->input->post('message') . '
				--------------- RESERVATION DATA ---------------<br /><br>
				Thank you for your prompt attention to the above, I look forward to receiveing a letter confirming my reservation.<br><br>
				Kind Regards,<br>
				' . $this->input->post('name') .'<br>
				' . $this->input->post('email') . '
				'
	        );
	 
	        if ($this->email->send())
	        {
	             
	        // Konfigurasi email.
	        $config = [
	           'useragent' => 'CodeIgniter',
	           'protocol'  => 'smtp',
	           'mailpath'  => '/usr/sbin/sendmail',
	           'smtp_host' => 'ssl://smtp.gmail.com',
	           'smtp_user' => 'agusdownload5293@gmail.com',   // Ganti dengan email gmail Anda.
	           'smtp_pass' => 'agusarim',             // Password gmail Anda.
	           'smtp_port' => 465,
	           'smtp_keepalive' => TRUE,
	           'smtp_crypto' => 'SSL',
	           'wordwrap'  => TRUE,
	           'wrapchars' => 80,
	           'mailtype'  => 'html',
	           'charset'   => 'utf-8',
	           'validate'  => TRUE,
	           'crlf'      => "\r\n",
	           'newline'   => "\r\n",
	        ];
	 
	        // Load library email dan konfigurasinya.
	        $this->email->initialize($config);
	 
	        // Pengirim dan penerima email.
	        $this->email->from('info@samarihillvillas.com','Info Samari Hill Villas');    // Email dan nama pegirim.
	        $this->email->to($this->input->post('email'));   // Penerima email.
	 
	        // Lampiran email. Isi dengan url/path file.
	        // $this->email->attach('https://masrud.com/themes/masrud/img/logo.png');
	 
	        // Subject email.
	        $this->email->subject('Auto-replay');
	 
	        // Isi email. Bisa dengan format html.
	        $this->email->message(
	        	'Dear, ' . $this->input->post('name') . ' <br/><br/>
	        	You has been booking Samari Hill Vilas <br />
	        	Please wait a momment, until reply your booking from us. <br /><br />
	        	Regards,
	        	<br />
	        	Administrator of <a href="http://samarihillvillas.com">Samari Hill Villas</a>'
	        	);
	 
	        $this->email->send();
	            echo "<script>alert('Your reservation has been send, wait a while for reply from us');history.go(-1);</script>";
	            
	        }
	        else
	        {
	            echo "<script>alert('Failed');history.go(-1);</script>";
	        }
    	} else {
          echo "<script>alert('Your captcha keyword is wrong!');history.go(-1);</script>";
        }


    }
	

}
