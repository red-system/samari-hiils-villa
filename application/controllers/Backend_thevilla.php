<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_thevilla extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_villa()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadervilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillaheader');
		$data['title'] = 'Header The Villa Page';
		$data['link'] = 'header_villa_edit';
		$data['general_name'] = 'header_villa';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_villa_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheadervilla');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['imagevilla'] = $this->backend_thevillamodel->getrow_image('header_villa');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header The Villa Page';
		$data['general_name'] = 'header_villa';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevillaheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_villa()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillaabout');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('about_villa');
		$data['title'] = 'About Villa';
		$data['link'] = 'about_villa_edit';
		$data['general_name'] = 'about_villa';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function about_villa_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Villa';
		$data['general_name'] = 'about_villa';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_villa_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutvilla');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Villa';
		$data['general_name'] = 'about_villa';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function the_area()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thearea');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillaarea');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('the_area');
		$data['title'] = 'The Area';
		$data['link'] = 'the_area_edit';
		$data['general_name'] = 'the_area';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function the_area_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thearea');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'The Area';
		$data['general_name'] = 'the_area';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function the_area_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thearea');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'The Area';
		$data['general_name'] = 'the_area';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function the_building()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thebuilding');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillabuilding');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('the_building');
		$data['title'] = 'The Building';
		$data['link'] = 'the_building_edit';
		$data['general_name'] = 'the_building';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function the_building_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thebuilding');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'The Building';
		$data['general_name'] = 'the_building';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function the_building_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_thebuilding');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'The Building';
		$data['general_name'] = 'the_building';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function comfort()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_comfort');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['thevillas'] = $this->backend_thevillamodel->get_thevillaall('thevillacomfort');
		$data['imagevilla'] = $this->backend_thevillamodel->get_image('comfort');
		$data['title'] = 'Comfort';
		$data['link'] = 'comfort_edit';
		$data['general_name'] = 'comfort';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevilla');
	    $this->load->view('backend/templates/footer');
	}

	public function comfort_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_comfort');

		$data['thevilla'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Comfort';
		$data['general_name'] = 'comfort';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/thevilla_edit');
		$this->load->view('backend/templates/footer');
	}

	public function comfort_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_comfort');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Comfort';
		$data['general_name'] = 'comfort';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/thevillaimage_add');
	    $this->load->view('backend/templates/footer');
	}	

	public function update_thevilla()
	{
		$res=$this->backend_thevillamodel->update_thevilla();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderthevilla()
	{
		$this->backend_thevillamodel->update_thevilla();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
           	$config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_thevillamodel->get_thevillaimage_by_refid('header_villa');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_thevillamodel->update_headerthevilla($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/about';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/about/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 960;
            $config1['height']= 720;
            $config1['new_image']= './assets/images/about/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_thevillamodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_thevillamodel->get_thevillaimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/about/'.$dataimage1);
        $this->backend_thevillamodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	public function team_member()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_teammember');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['teams'] = $this->backend_thevillamodel->get_thevillaall('team_member');
		
		$data['title'] = 'Team Member';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/teammember');
	    $this->load->view('backend/templates/footer');
	}

	public function team_member_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_teammember');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Team Member';

		$this->load->view('backend/templates/header',$data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/teammember_add');
		$this->load->view('backend/templates/footer');
	}

	public function team_member_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_teammember');
		
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['team'] = $this->backend_thevillamodel->get_thevilla_by_id($id);
		$data['title'] = 'Team Member';
		
		$this->load->view('backend/templates/header',$data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/teammember_edit');
		$this->load->view('backend/templates/footer');
	}

	public function team_member_addprocess()
	{
		$config['upload_path']          = './assets/images/team/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);

		if(!$this->upload->do_upload('picture')){
			$data['file_name']= "nopic.png";
			$res=$this->backend_thevillamodel->team_member_add($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/team/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 201;
            $config1['height']= 201;
            $config1['new_image']= './assets/images/team/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data['file_name']= $gbr['file_name'];
           	$res=$this->backend_thevillamodel->team_member_add($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
	             	<i class="icon-remove"></i>
	             </button>
	             <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Add done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
			
		}

		redirect('backend_thevilla/team_member');
	}


	public function team_member_editprocess($id)
	{
		$general_id = $this->input->post('general_id');

		$config['upload_path']          = './assets/images/team/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if((!$this->upload->do_upload('picture'))){
			$res=$this->backend_thevillamodel->update_team_member();
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$gbr = $this->upload->data();
			//Compress Image
           	$config1['image_library']='gd2';
            $config1['source_image']='./assets/images/team/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 201;
            $config1['height']= 201;
            $config1['new_image']= './assets/images/team/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
           
            $pic= $this->backend_thevillamodel->get_thevilla_by_id($general_id);
            $datapic = $pic['main_image'];
           
            if ($datapic=='nopic.png') {
            	$res=$this->backend_thevillamodel->update_team_member($data);
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
		             	<i class="icon-remove"></i>
		             </button>
		             <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
            } else {
	            unlink('./assets/images/team/'.$datapic);
	            $res=$this->backend_thevillamodel->update_team_member($data);
				if($res==true)
				{
				  $this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
		             	<i class="icon-remove"></i>
		             </button>
		             <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Update done!!!
					 </p>');
				}
				else
				{
				  $this->session->set_flashdata('err', "Update failed!!!");
				}
			}
		}

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function team_member_delete($id)
	{
		$pic= $this->backend_thevillamodel->get_thevilla_by_id($id);
        $datapic = $pic['main_image'];
        
        if ($datapic=='nopic.png'){
        	$this->backend_thevillamodel->delete_team_member($id);
			$this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Delete done!!!
					 </p>');
        } else {
	        unlink('./assets/images/team/'.$datapic);
	        $this->backend_thevillamodel->delete_team_member($id);
			$this->session->set_flashdata('true', 
				  	'<button data-dismiss="alert" class="close close-sm" type="button">
	                 	<i class="icon-remove"></i>
	                 </button>
	                 <h4>
						<i class="icon-ok-sign"></i>
						Success!
					 </h4> 
					 <p>Delete done!!!
					 </p>');
		}
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

}