<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }

	function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}
    
	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'home');

		//def
		$data['lang'] = $this->lang->lang();
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		//end of def

		//content
		$data['viewdetail'] = $this->backend_menutitlemodel->getrow_menutitlefront('viewdetail',$data['lang']);
		$data['picture'] = $this->backend_homemodel->get_homerow('slide');
		$data['icon'] = $this->backend_homemodel->get_homerow('icon');
		$data['homepicture'] = $this->backend_homemodel->getrow_homefront('homepicture',$data['lang']);
		$data['menulink1'] = $this->backend_homemodel->getrow_homefront('homemenulink1',$data['lang']);
		$data['image1'] = $this->backend_homemodel->getrow_image('homemenulink1');
		$data['menulink2'] = $this->backend_homemodel->getrow_homefront('homemenulink2',$data['lang']);
		$data['image2'] = $this->backend_homemodel->getrow_image('homemenulink2');
		$data['menulink3'] = $this->backend_homemodel->getrow_homefront('homemenulink3',$data['lang']);
		$data['image3'] = $this->backend_homemodel->getrow_image('homemenulink3');
		$data['guestbooklists'] = $this->backend_guestbookmodel->get_guestbookhome('guestbooklist');
		$data['guestbookimages'] = $this->backend_homemodel->get_image('guestbook_gallery');
		$data['aboutus'] = $this->backend_homemodel->getrow_homefront('homeaboutus',$data['lang']);
		$data['imageaboutus'] = $this->backend_homemodel->get_image('homeaboutus');
		$data['ourbest'] = $this->backend_homemodel->getrow_homefront('homeourbest',$data['lang']);
		$data['imageourbest'] = $this->backend_homemodel->get_image('homeourbest');
		$data['feature1'] = $this->backend_homemodel->getrow_homefront('feature1',$data['lang']);
		$data['featureimg1'] = $this->backend_homemodel->getrow_image('feature1');
		$data['feature2'] = $this->backend_homemodel->getrow_homefront('feature2',$data['lang']);
		$data['featureimg2'] = $this->backend_homemodel->getrow_image('feature2');
		$data['feature3'] = $this->backend_homemodel->getrow_homefront('feature3',$data['lang']);
		$data['featureimg3'] = $this->backend_homemodel->getrow_image('feature3');
		$data['feature4'] = $this->backend_homemodel->getrow_homefront('feature4',$data['lang']);
		$data['featureimg4'] = $this->backend_homemodel->getrow_image('feature4');
		$data['feature5'] = $this->backend_homemodel->getrow_homefront('feature5',$data['lang']);
		$data['featureimg5'] = $this->backend_homemodel->getrow_image('feature5');
		$data['feature6'] = $this->backend_homemodel->getrow_homefront('feature6',$data['lang']);
		$data['featureimg6'] = $this->backend_homemodel->getrow_image('feature6');
		
		$this->load->view('templates/header',$data);
		$this->load->view('home');
		$this->load->view('templates/footer');
	}

	

}
