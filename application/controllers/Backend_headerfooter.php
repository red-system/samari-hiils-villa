<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_headerfooter extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_headerfooter');

		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['quoteen'] = $this->backend_headerfootermodel->get_headerfooter('quoteen');
		$data['quotede'] = $this->backend_headerfootermodel->get_headerfooter('quotede');
		$data['buttonen'] = $this->backend_headerfootermodel->get_headerfooter('buttonen');
		$data['buttonde'] = $this->backend_headerfootermodel->get_headerfooter('buttonde');
		$data['facebook'] = $this->backend_headerfootermodel->get_sosiallink('Facebook');
		$data['instagram'] = $this->backend_headerfootermodel->get_sosiallink('Instagram');
		$data['twitter'] = $this->backend_headerfootermodel->get_sosiallink('Twitter');
		$data['youtube'] = $this->backend_headerfootermodel->get_sosiallink('Youtube');

		$data['profile'] = $this->backend_profile_model->get_profile('admin');

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/headerfooter');
	    $this->load->view('backend/templates/footer');
	}

	public function update_headerfooter(){
		$this->backend_headerfootermodel->update_address();
		$this->backend_headerfootermodel->update_phone1();
		$this->backend_headerfootermodel->update_phone2();
		$this->backend_headerfootermodel->update_email();
		$this->backend_headerfootermodel->update_quoteen();
		$this->backend_headerfootermodel->update_quotede();
		$this->backend_headerfootermodel->update_buttonen();
		$this->backend_headerfootermodel->update_buttonde();
		$this->backend_headerfootermodel->update_facebook();
		$this->backend_headerfootermodel->update_instagram();
		$this->backend_headerfootermodel->update_twitter();
		$this->backend_headerfootermodel->update_youtube();
		
		$config['upload_path']          = './assets/images';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('logoheader')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= true;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_headerfootermodel->get_headerfooter('logo_header');
            $datalogo = $logo['main_image'];
            unlink('./assets/images/'.$datalogo);
			$this->backend_headerfootermodel->update_logoheader($data);
		}

		$config['upload_path']          = './assets/images';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('logofooter')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 145;
            $config1['height']= 149;
            $config1['new_image']= './assets/images/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_headerfootermodel->get_headerfooter('logo_footer');
            $datalogo = $logo['main_image'];
            unlink('./assets/images/'.$datalogo);
			$this->backend_headerfootermodel->update_logofooter($data);
		}
			
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
}