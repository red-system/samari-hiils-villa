<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_menu_title extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function index()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menutitle');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['viewdetailen'] = $this->backend_menutitlemodel->get_menutitle('viewdetailen');
		$data['viewdetailde'] = $this->backend_menutitlemodel->get_menutitle('viewdetailde');
		$data['homeen'] = $this->backend_menutitlemodel->get_menutitle('homeen');
		$data['homede'] = $this->backend_menutitlemodel->get_menutitle('homede');
		$data['thevillaen'] = $this->backend_menutitlemodel->get_menutitle('thevillaen');
		$data['thevillade'] = $this->backend_menutitlemodel->get_menutitle('thevillade');
		$data['locationen'] = $this->backend_menutitlemodel->get_menutitle('locationen');
		$data['locationde'] = $this->backend_menutitlemodel->get_menutitle('locationde');
		$data['offersen'] = $this->backend_menutitlemodel->get_menutitle('offersen');
		$data['offersde'] = $this->backend_menutitlemodel->get_menutitle('offersde');
		$data['galleryen'] = $this->backend_menutitlemodel->get_menutitle('galleryen');
		$data['galleryde'] = $this->backend_menutitlemodel->get_menutitle('galleryde');
		$data['availabilityen'] = $this->backend_menutitlemodel->get_menutitle('availabilityen');
		$data['availabilityde'] = $this->backend_menutitlemodel->get_menutitle('availabilityde');
		$data['guestbooken'] = $this->backend_menutitlemodel->get_menutitle('guestbooken');
		$data['guestbookde'] = $this->backend_menutitlemodel->get_menutitle('guestbookde');
		$data['pricingen'] = $this->backend_menutitlemodel->get_menutitle('pricingen');
		$data['pricingde'] = $this->backend_menutitlemodel->get_menutitle('pricingde');
		$data['contacten'] = $this->backend_menutitlemodel->get_menutitle('contacten');
		$data['contactde'] = $this->backend_menutitlemodel->get_menutitle('contactde');
		$data['teammemberen'] = $this->backend_menutitlemodel->get_menutitle('teammemberen');
		$data['teammemberde'] = $this->backend_menutitlemodel->get_menutitle('teammemberde');
		$data['groundplanen'] = $this->backend_menutitlemodel->get_menutitle('groundplanen');
		$data['groundplande'] = $this->backend_menutitlemodel->get_menutitle('groundplande');
		

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/menutitle');
	    $this->load->view('backend/templates/footer');
	}

	public function update_menutitle(){
		$this->backend_menutitlemodel->update_viewdetailen();
		$this->backend_menutitlemodel->update_viewdetailde();
		$this->backend_menutitlemodel->update_homeen();
		$this->backend_menutitlemodel->update_homede();
		$this->backend_menutitlemodel->update_thevillaen();
		$this->backend_menutitlemodel->update_thevillade();
		$this->backend_menutitlemodel->update_locationen();
		$this->backend_menutitlemodel->update_locationde();
		$this->backend_menutitlemodel->update_offersen();
		$this->backend_menutitlemodel->update_offersde();
		$this->backend_menutitlemodel->update_galleryen();
		$this->backend_menutitlemodel->update_galleryde();
		$this->backend_menutitlemodel->update_availabilityen();
		$this->backend_menutitlemodel->update_availabilityde();
		$this->backend_menutitlemodel->update_guestbooken();
		$this->backend_menutitlemodel->update_guestbookde();
		$this->backend_menutitlemodel->update_pricingen();
		$this->backend_menutitlemodel->update_pricingde();
		$this->backend_menutitlemodel->update_contacten();
		$this->backend_menutitlemodel->update_contactde();
		$this->backend_menutitlemodel->update_teammemberen();
		$this->backend_menutitlemodel->update_teammemberde();
		$this->backend_menutitlemodel->update_groundplanen();
		$this->backend_menutitlemodel->update_groundplande();
				
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
}