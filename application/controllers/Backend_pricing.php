<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_pricing extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function header_pricing()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderpricing');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricings'] = $this->backend_pricingmodel->get_pricingall('pricingheader');
		$data['title'] = 'Header Pricing Page';
		$data['link'] = 'header_pricing_edit';
		$data['general_name'] = 'header_pricing';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricingheader');
	    $this->load->view('backend/templates/footer');
	}

	public function header_pricing_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pageheaderpricing');

		$data['pricing'] = $this->backend_pricingmodel->get_pricing_by_id($id);
		$data['imagepricing'] = $this->backend_pricingmodel->getrow_image('header_pricing');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Header Pricing Page';
		$data['general_name'] = 'header_pricing';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/pricingheader_edit');
		$this->load->view('backend/templates/footer');
	}

	public function first_feeling()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_firstfeeling');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricings'] = $this->backend_pricingmodel->get_pricingall('pricingfeelingfirst');
		$data['imagepricing'] = $this->backend_pricingmodel->get_image('first_feeling');
		$data['title'] = 'First Feeling';
		$data['link'] = 'first_feeling_edit';
		$data['general_name'] = 'first_feeling';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricing');
	    $this->load->view('backend/templates/footer');
	}

	public function first_feeling_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_firstfeeling');

		$data['pricing'] = $this->backend_pricingmodel->get_pricing_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'First Feeling';
		$data['general_name'] = 'first_feeling';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/pricing_edit');
		$this->load->view('backend/templates/footer');
	}

	public function first_feeling_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_firstfeeling');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'First Feeling';
		$data['general_name'] = 'first_feeling';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricingimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function second_feeling()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_secondfeeling');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricings'] = $this->backend_pricingmodel->get_pricingall('pricingfeelingsecond');
		$data['imagepricing'] = $this->backend_pricingmodel->get_image('second_feeling');
		$data['title'] = 'Second Feeling';
		$data['link'] = 'second_feeling_edit';
		$data['general_name'] = 'second_feeling';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricing');
	    $this->load->view('backend/templates/footer');
	}

	public function second_feeling_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_secondfeeling');

		$data['pricing'] = $this->backend_pricingmodel->get_pricing_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Second Feeling';
		$data['general_name'] = 'second_feeling';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/pricing_edit');
		$this->load->view('backend/templates/footer');
	}

	public function second_feeling_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_secondfeeling');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Second Feeling';
		$data['general_name'] = 'second_feeling';

		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricingimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function pricelist()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_pricelist');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['pricing'] = $this->backend_pricingmodel->getrow_pricingall('pricinglist');
		$data['title'] = 'Pricelist';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/pricelist');
	    $this->load->view('backend/templates/footer');
	}

	public function update_pricelist()
	{
		//start
		$config['upload_path']          = './assets';
		$config['allowed_types']        = '*';
		$config['max_size']             = 10000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('file')){
			$res=$this->backend_pricingmodel->update_pricelist();
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		} else {
			$data['file_name']= $this->upload->data('file_name');
			$file= $this->backend_pricingmodel->getrow_pricingall('pricinglist');
            $datafile = $file['main_image'];
            unlink('./assets/'.$datafile);
			$res=$this->backend_pricingmodel->update_pricelist($data);
			if($res==true)
			{
			  $this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Update done!!!
				 </p>');
			}
			else
			{
			  $this->session->set_flashdata('err', "Update failed!!!");
			}
		}
		//end

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pricing()
	{
		$res=$this->backend_pricingmodel->update_pricing();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function update_pageheaderpricing()
	{
		$this->backend_pricingmodel->update_pricing();

		$config['upload_path']          = './assets/images/banner/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/banner/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 1920;
            $config1['new_image']= './assets/images/banner/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_pricingmodel->get_pricingimage_by_refid('header_pricing');
            $datapic = $pic['picture_name'];
            unlink('./assets/images/banner/'.$datapic);
			$this->backend_pricingmodel->update_headerpricing($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());

	}

	public function image_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/pricing';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/pricing/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 960;
            $config1['height']= 720;
            $config1['new_image']= './assets/images/pricing/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_pricingmodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_pricingmodel->get_pricingimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/pricing/'.$dataimage1);
        $this->backend_pricingmodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	
	

}