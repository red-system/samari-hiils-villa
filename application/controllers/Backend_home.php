<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_home extends CI_Controller {
	
	public function __construct()
    {
        parent::__construct();
        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
        {
			redirect('user_auth');
        }
    }

	public function homepicture()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_homepicture');

		$data['picture'] = $this->backend_homemodel->get_homerow('slide');
		$data['icon'] = $this->backend_homemodel->get_homerow('icon');
		$data['homepictureens'] = $this->backend_homemodel->get_homearrayen('homepicture');
		$data['homepicturedes'] = $this->backend_homemodel->get_homearrayde('homepicture');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/homepicture');
	    $this->load->view('backend/templates/footer');
	}

	public function update_homepicture(){
		$this->backend_homemodel->update_titleen();
		$this->backend_homemodel->update_titlede();
		
		$config['upload_path']          = './assets/images/slider';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 50000;
		$config['max_width']            = 50000;
		$config['max_height']           = 50000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('picture')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/slider/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 1920;
            $config1['height']= 980;
            $config1['new_image']= './assets/images/slider/'.$gbr['file_name'];
           	$this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $image= $this->backend_homemodel->get_homerow('slide');
            $dataimage = $image['main_image'];
            unlink('./assets/images/slider/'.$dataimage);
			$this->backend_homemodel->update_picture($data);
		}

		$config['upload_path']          = './assets/images/slider';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/slider/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= TRUE;
            $config1['width']= 136;
            $config1['height']= 136;
            $config1['new_image']= './assets/images/slider/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $image= $this->backend_homemodel->get_homerow('icon');
            $dataimage = $image['main_image'];
            unlink('./assets/images/slider/'.$dataimage);
			$this->backend_homemodel->update_icon($data);
		}
			
		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}


	public function menulink1()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink1');

		$data['menulinks'] = $this->backend_homemodel->get_homeall('homemenulink1');
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink1');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 1';
		$data['link'] = 'menulink1_edit';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/menulink');
	    $this->load->view('backend/templates/footer');
	}

	public function menulink1_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink1');

		$data['menulink'] = $this->backend_homemodel->get_home_by_id($id);
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink1');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 1';
		$data['general_name'] = 'menulink1';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/menulink_edit');
		$this->load->view('backend/templates/footer');
	}

	public function menulink2()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink2');

		$data['menulinks'] = $this->backend_homemodel->get_homeall('homemenulink2');
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink2');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 2';
		$data['link'] = 'menulink2_edit';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/menulink');
	    $this->load->view('backend/templates/footer');
	}

	public function menulink2_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink2');

		$data['menulink'] = $this->backend_homemodel->get_home_by_id($id);
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink2');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 2';
		$data['general_name'] = 'menulink2';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/menulink_edit');
		$this->load->view('backend/templates/footer');
	}

	public function menulink3()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink3');

		$data['menulinks'] = $this->backend_homemodel->get_homeall('homemenulink3');
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink3');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 3';
		$data['link'] = 'menulink3_edit';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/menulink');
	    $this->load->view('backend/templates/footer');
	}

	public function menulink3_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_menulink3');

		$data['menulink'] = $this->backend_homemodel->get_home_by_id($id);
		$data['image'] = $this->backend_homemodel->getrow_image('homemenulink3');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Menu Link 3';
		$data['general_name'] = 'menulink3';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/menulink_edit');
		$this->load->view('backend/templates/footer');
	}

	public function update_menulink()
	{
		$this->backend_homemodel->update_menulink();

		$id = $this->input->post('id');

		$config['upload_path']          = './assets/images/home/';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 800;
            $config1['height']= 450;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $pic= $this->backend_homemodel->get_homeimage_by_id($id);
            $datapic = $pic['picture_name'];
            unlink('./assets/images/home/'.$datapic);
			$this->backend_homemodel->update_menulinkimage($data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		

		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function guestbook_gallery()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookgallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['images'] = $this->backend_homemodel->get_image('guestbook_gallery');
		$data['title'] = 'Guest Book Gallery';
		$data['general_name'] = 'guestbook_gallery';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/guestbookgallery');
	    $this->load->view('backend/templates/footer');
	}

	public function guestbook_gallery_image_add()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_guestbookgallery');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['images'] = $this->backend_homemodel->get_image('guestbook_gallery');
		$data['title'] = 'Guest Book Gallery';
		$data['general_name'] = 'guestbook_gallery';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/homeimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function guestbook_gallery_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/guestbook';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/guestbook/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 480;
            $config1['height']= 270;
            $config1['new_image']= './assets/images/guestbook/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_homemodel->image_add($data);
		}
	}

	public function image_delete($id)
	{
		$image= $this->backend_homemodel->get_homeimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/guestbook/'.$dataimage1);
        $this->backend_homemodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function imagehome_delete($id)
	{
		$image= $this->backend_homemodel->get_homeimage_by_id($id);
        $dataimage1 = $image['picture_name'];
       	unlink('./assets/images/home/'.$dataimage1);
        $this->backend_homemodel->image_delete($id);
		$this->session->set_flashdata('true', 
			  	'<button data-dismiss="alert" class="close close-sm" type="button">
                 	<i class="icon-remove"></i>
                 </button>
                 <h4>
					<i class="icon-ok-sign"></i>
					Success!
				 </h4> 
				 <p>Delete image done!!!
				 </p>');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function about_us()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutus');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['homes'] = $this->backend_homemodel->get_homeall('homeaboutus');
		$data['imagehome'] = $this->backend_homemodel->get_image('homeaboutus');
		$data['title'] = 'About Us';
		$data['link'] = 'about_us_edit';
		$data['general_name'] = 'about_us';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/aboutus');
	    $this->load->view('backend/templates/footer');
	}

	public function about_us_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutus');

		$data['home'] = $this->backend_homemodel->get_home_by_id($id);
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Us';
		$data['general_name'] = 'about_us';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/aboutus_edit');
		$this->load->view('backend/templates/footer');
	}

	public function about_us_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_aboutus');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'About Us';
		$data['link'] = 'about_us';
		$data['general_name'] = 'homeaboutus';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/aboutusimage_add');
	    $this->load->view('backend/templates/footer');
	}	

	public function update_aboutus()
	{
		$res=$this->backend_homemodel->update_aboutus();
		if($res==true)
		{
		  $this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		}
		else
		{
		  $this->session->set_flashdata('err', "Update failed!!!");
		}
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}

	public function aboutusimage_addprocess()
	{
		$general_name = $this->input->post('general_name');

		$post = $this->input->post();
	
		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;

		$this->upload->initialize($config);
		if($this->upload->do_upload('file'))
		{
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 960;
            $config1['height']= 720;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

           	$data = array(	'picture_name'		=> $gbr['file_name'],
							'general_ref_id'	=> $general_name,
						);
			$this->backend_homemodel->image_add($data);
		}
	}

	public function our_best()
	{
		
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_ourbest');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['homes'] = $this->backend_homemodel->get_homeall('homeourbest');
		$data['imagehome'] = $this->backend_homemodel->get_image('homeourbest');
		$data['title'] = 'Our Best';
		$data['link'] = 'our_best_edit';
		$data['general_name'] = 'our_best';
		
		$this->load->view('backend/templates/header', $data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/ourbest');
	    $this->load->view('backend/templates/footer');
	}

	public function our_best_edit($id)
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_ourbest');

		$data['ourbest'] = $this->backend_homemodel->get_home_by_id($id);
		$data['image'] = $this->backend_homemodel->getrow_image('homeourbest');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['feature1'] = $this->backend_homemodel->get_homefeaturerow($id,'feature1');
		$data['featureimg1'] = $this->backend_homemodel->getrow_image('feature1');
		$data['feature2'] = $this->backend_homemodel->get_homefeaturerow($id,'feature2');
		$data['featureimg2'] = $this->backend_homemodel->getrow_image('feature2');
		$data['feature3'] = $this->backend_homemodel->get_homefeaturerow($id,'feature3');
		$data['featureimg3'] = $this->backend_homemodel->getrow_image('feature3');
		$data['feature4'] = $this->backend_homemodel->get_homefeaturerow($id,'feature4');
		$data['featureimg4'] = $this->backend_homemodel->getrow_image('feature4');
		$data['feature5'] = $this->backend_homemodel->get_homefeaturerow($id,'feature5');
		$data['featureimg5'] = $this->backend_homemodel->getrow_image('feature5');
		$data['feature6'] = $this->backend_homemodel->get_homefeaturerow($id,'feature6');
		$data['featureimg6'] = $this->backend_homemodel->getrow_image('feature6');
		$data['title'] = 'Our Best';
		$data['general_name'] = 'our_best';
		
		$this->load->view('backend/templates/header', $data);
		$this->load->view('backend/templates/menu');
		$this->load->view('backend/ourbest_edit');
		$this->load->view('backend/templates/footer');
	}

	public function our_best_image_add()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'backend_ourbest');

		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['profile'] = $this->backend_profile_model->get_profile('admin');
		$data['title'] = 'Our Best';
		$data['link'] = 'our_best';
		$data['general_name'] = 'homeourbest';
		
		$this->load->view('backend/templates/header',$data);
	    $this->load->view('backend/templates/menu');
	    $this->load->view('backend/aboutusimage_add');
	    $this->load->view('backend/templates/footer');
	}

	public function update_ourbest()
	{
		$this->backend_homemodel->update_ourbest();
		$this->backend_homemodel->update_feature1();
		$this->backend_homemodel->update_feature2();
		$this->backend_homemodel->update_feature3();
		$this->backend_homemodel->update_feature4();
		$this->backend_homemodel->update_feature5();
		$this->backend_homemodel->update_feature6();

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('image')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 960;
            $config1['height']= 720;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('homeourbest');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('homeourbest',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon1')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature1');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature1',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon2')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature2');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature2',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon3')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature3');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature3',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon4')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature4');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature4',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon5')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature5');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature5',$data);
		}

		$config['upload_path']          = './assets/images/home';
		$config['allowed_types']        = 'gif|jpg|png|jpeg';
		$config['max_size']             = 10000;
		$config['max_width']            = 20000;
		$config['max_height']           = 20000;
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('icon6')){
			$errors = array('error' => $this->upload->display_errors());
		} else {
			$gbr = $this->upload->data();
			//Compress Image
            $config1['image_library']='gd2';
            $config1['source_image']='./assets/images/home/'.$gbr['file_name'];
            $config1['create_thumb']= FALSE;
            $config1['maintain_ratio']= FALSE;
            $config1['width']= 48;
            $config1['height']= 48;
            $config1['new_image']= './assets/images/home/'.$gbr['file_name'];
            $this->image_lib->initialize($config1);
		 	$this->image_lib->resize();
		 	$this->image_lib->clear();

            $data['file_name']= $gbr['file_name'];
            $logo= $this->backend_homemodel->getrow_image('feature6');
            $datalogo = $logo['picture_name'];
            unlink('./assets/images/home/'.$datalogo);
			$this->backend_homemodel->update_featureicon('feature6',$data);
		}

		$this->session->set_flashdata('true', 
		  	'<button data-dismiss="alert" class="close close-sm" type="button">
             	<i class="icon-remove"></i>
             </button>
             <h4>
				<i class="icon-ok-sign"></i>
				Success!
			 </h4> 
			 <p>Update done!!!
			 </p>');
		
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}	

}