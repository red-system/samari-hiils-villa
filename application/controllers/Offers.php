<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offers extends CI_Controller {

	// public function __construct()
 //    {
 //        parent::__construct();
 //        if(!$user = $this->session->userdata('email'))  // if you add in constructor no need write each function in above controller.
 //        {
	// 		redirect('user_auth');
 //        }
 //    }
    function __construct(){
		parent:: __construct();
		$this->load->helper(array('form', 'url', 'html','language'));
	}

	public function index()
	{
		$this->session->unset_userdata('menu');
		$this->session->set_userdata('menu', 'offers');

		//def
		$data['lang'] = $this->lang->lang();
		$data['address'] = $this->backend_headerfootermodel->get_headerfooter('address');
		$data['phone1'] = $this->backend_headerfootermodel->get_headerfooter('phone1');
		$data['phone2'] = $this->backend_headerfootermodel->get_headerfooter('phone2');
		$data['email'] = $this->backend_headerfootermodel->get_headerfooter('email');
		$data['logoheader'] = $this->backend_headerfootermodel->get_headerfooter('logo_header');
		$data['logofooter'] = $this->backend_headerfootermodel->get_headerfooter('logo_footer');
		$data['social_medias'] = $this->backend_headerfootermodel->get_sosiallink();
		$data['quote'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterquote',$data['lang']);
		$data['button'] = $this->backend_headerfootermodel->getrow_headerfooterfront('headerfooterbutton',$data['lang']);
		$data['home'] = $this->backend_menutitlemodel->getrow_menutitlefront('home',$data['lang']);
		$data['thevilla'] = $this->backend_menutitlemodel->getrow_menutitlefront('thevilla',$data['lang']);
		$data['location'] = $this->backend_menutitlemodel->getrow_menutitlefront('location',$data['lang']);
		$data['offers'] = $this->backend_menutitlemodel->getrow_menutitlefront('offers',$data['lang']);
		$data['gallery'] = $this->backend_menutitlemodel->getrow_menutitlefront('gallery',$data['lang']);
		$data['availability'] = $this->backend_menutitlemodel->getrow_menutitlefront('availability',$data['lang']);
		$data['guestbook'] = $this->backend_menutitlemodel->getrow_menutitlefront('guestbook',$data['lang']);
		$data['pricing'] = $this->backend_menutitlemodel->getrow_menutitlefront('pricing',$data['lang']);
		$data['contact'] = $this->backend_menutitlemodel->getrow_menutitlefront('contact',$data['lang']);
		//end of def

		//content
		$data['header'] = $this->backend_offersmodel->getrow_offersfront('offersheader',$data['lang']);
		$data['imageheader'] = $this->backend_offersmodel->getrow_image('header_offers');
		$data['first'] = $this->backend_offersmodel->getrow_offersfront('offersfirst',$data['lang']);
		$data['imagefirst'] = $this->backend_offersmodel->get_image('offers_first');
		$data['second'] = $this->backend_offersmodel->getrow_offersfront('offerssecond',$data['lang']);
		$data['imagesecond'] = $this->backend_offersmodel->get_image('offers_second');
		$data['third'] = $this->backend_offersmodel->getrow_offersfront('offersthird',$data['lang']);
		$data['imagethird'] = $this->backend_offersmodel->get_image('offers_third');
		$data['fourth'] = $this->backend_offersmodel->getrow_offersfront('offersfourth',$data['lang']);
		$data['imagefourth'] = $this->backend_offersmodel->get_image('offers_fourth');
		$data['fifth'] = $this->backend_offersmodel->getrow_offersfront('offersfifth',$data['lang']);
		$data['imagefifth'] = $this->backend_offersmodel->get_image('offers_fifth');
		$data['sixth'] = $this->backend_offersmodel->getrow_offersfront('offerssixth',$data['lang']);
		$data['imagesixth'] = $this->backend_offersmodel->get_image('offers_sixth');
		$data['seventh'] = $this->backend_offersmodel->getrow_offersfront('offersseventh',$data['lang']);
		$data['imageseventh'] = $this->backend_offersmodel->get_image('offers_seventh');
		$data['eighth'] = $this->backend_offersmodel->getrow_offersfront('offerseighth',$data['lang']);
		$data['imageeighth'] = $this->backend_offersmodel->get_image('offers_eighth');
		//end of content

		$this->load->view('templates/header',$data);
		$this->load->view('offers');
		$this->load->view('templates/footer');
	}

	

}
