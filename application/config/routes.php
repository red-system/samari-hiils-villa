<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';


$langs = array('en/','de/');

foreach($langs as $lang) {
	$route[$lang.'the-villa'] = "the_villa";
	$route[$lang.'location'] = "location";
	$route[$lang.'offers'] = "offers";
	$route[$lang.'gallery'] = "gallery";
	$route[$lang.'availability'] = "availability";
	$route[$lang.'guest-book'] = "guest_book";
	$route[$lang.'guest-book/guestbook-addprocess'] = "guest_book/guestbook_addprocess";
	$route[$lang.'guest-book/index/(:any)'] = "guest_book/index/$1";
	$route[$lang.'pricing'] = "pricing";
	$route[$lang.'contact'] = "contact";
	$route[$lang.'contact/send-reservation'] = "contact/send_reservation";
}

$route['^en$'] = $route['default_controller'];
$route['^de$'] = $route['default_controller'];

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
