-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2019 at 11:07 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_samarihillvillas`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `artikel_id` int(5) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `kategori_id` int(5) NOT NULL,
  `sub_id` int(11) DEFAULT NULL,
  `username` varchar(80) NOT NULL,
  `artikel_title` varchar(100) NOT NULL,
  `artikel_gambar` varchar(400) NOT NULL,
  `artikel_waktu` varchar(50) NOT NULL,
  `artikel_isi` text NOT NULL,
  `artikel_label` text,
  `artikel_thumb_desc` text,
  `artikel_alamat` varchar(100) DEFAULT NULL,
  `artikel_remark` text NOT NULL,
  `artikel_destination` varchar(100) DEFAULT NULL,
  `artikel_durasi` varchar(100) DEFAULT NULL,
  `artikel_person` varchar(40) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `special` enum('no','yes') DEFAULT NULL,
  `link` varchar(40) NOT NULL,
  `posisi` varchar(40) NOT NULL,
  `publish` enum('no','yes') DEFAULT NULL,
  `permalink` text,
  `artikel_gambar_desc` varchar(255) NOT NULL,
  `artikel_short_desc` text NOT NULL,
  `visiting_list_artikel_id` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_day`
--

CREATE TABLE `tb_day` (
  `id_day` int(9) NOT NULL,
  `ref_id` int(9) NOT NULL,
  `day` date NOT NULL,
  `info` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_day`
--

INSERT INTO `tb_day` (`id_day`, `ref_id`, `day`, `info`) VALUES
(1, 1, '2019-04-16', 'checkIn'),
(2, 1, '2019-04-17', 'booked'),
(3, 1, '2019-04-18', 'booked'),
(4, 1, '2019-04-19', 'booked'),
(5, 1, '2019-04-20', 'checkOut'),
(6, 2, '2019-04-20', 'checkIn'),
(7, 2, '2019-04-21', 'booked'),
(8, 2, '2019-04-22', 'booked'),
(9, 2, '2019-04-23', 'booked'),
(10, 2, '2019-04-24', 'checkOut'),
(11, 3, '2019-04-30', 'checkIn'),
(12, 3, '2019-05-01', 'booked'),
(13, 3, '2019-05-02', 'booked'),
(14, 3, '2019-05-03', 'checkOut'),
(15, 4, '2019-05-15', 'checkIn'),
(16, 4, '2019-05-16', 'checkOut'),
(50, 5, '2019-06-16', 'checkIn'),
(51, 5, '2019-06-17', 'booked'),
(52, 5, '2019-06-18', 'booked'),
(53, 5, '2019-06-19', 'booked'),
(54, 5, '2019-06-20', 'booked'),
(55, 5, '2019-06-21', 'booked'),
(56, 5, '2019-06-22', 'booked'),
(57, 5, '2019-06-23', 'checkOut');

-- --------------------------------------------------------

--
-- Table structure for table `tb_general_data`
--

CREATE TABLE `tb_general_data` (
  `general_id` int(9) NOT NULL,
  `general_ref_id` int(9) NOT NULL,
  `oreder` int(9) NOT NULL,
  `general_name` varchar(200) NOT NULL,
  `general_sub_name` varchar(500) NOT NULL,
  `general_data` varchar(500) NOT NULL,
  `general_sub_data` varchar(500) NOT NULL,
  `general_desc` text NOT NULL,
  `general_sub_desc` text NOT NULL,
  `general_link` varchar(500) NOT NULL,
  `main_image` varchar(200) NOT NULL,
  `secondary_image` varchar(200) NOT NULL,
  `general_password` varchar(500) NOT NULL,
  `general_lang` varchar(100) NOT NULL,
  `general_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `general_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `general_additional_info` text NOT NULL,
  `facebook_link` text NOT NULL,
  `instagram_link` text NOT NULL,
  `twitter_link` text NOT NULL,
  `email_link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_general_data`
--

INSERT INTO `tb_general_data` (`general_id`, `general_ref_id`, `oreder`, `general_name`, `general_sub_name`, `general_data`, `general_sub_data`, `general_desc`, `general_sub_desc`, `general_link`, `main_image`, `secondary_image`, `general_password`, `general_lang`, `general_created_at`, `general_updated_at`, `general_additional_info`, `facebook_link`, `instagram_link`, `twitter_link`, `email_link`) VALUES
(1, 0, 0, 'headerfooter', 'address', 'Br.Dinas Antasari, Ds.Pacung, Kec.Tejakula, Buleleng, Bali', '', '', '', '', '', '', '', '', '2019-03-13 07:54:36', '2019-04-23 03:26:35', '', '', '', '', ''),
(2, 0, 0, 'headerfooter', 'phone1', '1-548-854-889', '', '', '', '', '', '', '', '', '2019-03-13 07:56:07', '2019-04-23 03:09:08', '', '', '', '', ''),
(3, 0, 0, 'headerfooter', 'email', 'hello@samarihillvillas.com', '', '', '', '', '', '', '', '', '2019-03-13 07:56:51', '2019-03-13 08:27:48', '', '', '', '', ''),
(4, 0, 0, 'headerfooter', 'logo_header', '', '', '', '', '', 'logo_panjang1.png', '', '', '', '2019-03-13 07:58:02', '2019-03-29 09:54:30', '', '', '', '', ''),
(5, 0, 0, 'headerfooter', 'logo_footer', '', '', '', '', '', 'Logo_SH_rund.png', '', '', '', '2019-03-13 07:58:54', '2019-03-29 09:51:54', '', '', '', '', ''),
(6, 0, 0, 'headerfooterquote', 'quoteen', '', '', 'Great Stay & Convenient Location', '', '', '', '', '', 'en', '2019-03-14 05:24:20', '2019-04-10 02:45:14', '', '', '', '', ''),
(7, 0, 0, 'headerfooterquote', 'quotede', '', '', 'Toller Aufenthalt und günstige Lage', '', '', '', '', '', 'de', '2019-03-14 05:24:40', '2019-04-10 02:45:18', '', '', '', '', ''),
(8, 0, 0, 'headerfooterbutton', 'buttonen', 'BOOK NOW', '', '', '', '', '', '', '', 'en', '2019-03-14 06:13:35', '2019-04-10 02:51:27', '', '', '', '', ''),
(9, 0, 0, 'headerfooterbutton', 'buttonde', 'JETZT BUCHEN', '', '', '', '', '', '', '', 'de', '2019-03-14 06:15:28', '2019-04-10 02:51:31', '', '', '', '', ''),
(10, 0, 0, 'home', 'slide', '', '', '', '', '', 'ads.jpg', '', '', '', '2019-03-15 07:50:55', '2019-04-24 05:53:51', '', '', '', '', ''),
(11, 0, 0, 'home', 'icon', '', '', '', '', '', 'Logo_SH_rund.png', '', '', '', '2019-03-15 07:51:50', '2019-03-29 09:50:28', '', '', '', '', ''),
(12, 0, 0, 'homepicture', 'homepicture', 'WELCOME TO', 'SAMARI HILL VILLAS', '', '', '', '', '', '', 'en', '2019-03-15 08:05:33', '2019-04-10 03:01:41', '', '', '', '', ''),
(13, 0, 0, 'homepicture', 'homepicture', 'WILLKOMMEN ZU', 'SAMARI HILL VILLAS', '', '', '', '', '', '', 'de', '2019-03-15 08:06:27', '2019-04-10 03:01:41', '', '', '', '', ''),
(14, 0, 0, 'homemenulink1', 'homemenulink1', 'DISCOVER BALI', '', '', '', 'location', '', '', '', 'en', '2019-03-25 07:25:38', '2019-04-10 03:45:08', '', '', '', '', ''),
(15, 0, 0, 'homemenulink1', 'homemenulink1', 'BALI ENTDECKEN', '', '', '', 'location', '', '', '', 'de', '2019-03-25 07:26:48', '2019-04-10 03:45:50', '', '', '', '', ''),
(16, 0, 0, 'homemenulink2', 'homemenulink2', 'EXPERIENCE CULTURE', '', '', '', 'offers', '', '', '', 'en', '2019-03-25 07:30:55', '2019-04-10 04:02:44', '', '', '', '', ''),
(17, 0, 0, 'homemenulink2', 'homemenulink2', 'KULTUR ERLEBEN', '', '', '', 'offers', '', '', '', 'de', '2019-03-25 07:31:45', '2019-04-10 04:02:42', '', '', '', '', ''),
(18, 0, 0, 'homemenulink3', 'homemenulink3', 'SENSE RELAXATION', '', '', '', 'the-villa', '', '', '', 'en', '2019-03-25 07:33:12', '2019-04-10 04:02:41', '', '', '', '', ''),
(19, 0, 0, 'homemenulink3', 'homemenulink3', 'ERHOLUNG FÜHLEN', '', '', '', 'the-villa', '', '', '', 'de', '2019-03-25 07:33:15', '2019-04-10 13:28:32', '', '', '', '', ''),
(20, 0, 0, 'homeaboutus', 'homeaboutus', 'ABOUT US', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source</p>\r\n', '', '', 'aboutus.jpg', '', '', 'en', '2019-03-27 04:20:16', '2019-04-10 14:39:58', '', '', '', '', ''),
(21, 0, 0, 'homeaboutus', 'homeaboutus', 'ÜBER UNS', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source</p>\r\n', '', '', 'aboutus1.jpg', '', '', 'de', '2019-03-27 04:20:38', '2019-04-10 14:32:47', '', '', '', '', ''),
(22, 0, 0, 'homeourbest', 'homeourbest', 'Our Best', '', '<p>One of Catalina Island&#39;s best-loved hotels, Hotel Vista Del Mar is recognized as one of Avalon&#39;s leading hotels with gracious island hospitality, thoughtful amenities and distinctive .One of Catalina Island&#39;s best-loved hotels, Hotel Vista Del Mar is recognized as one of Avalon&#39;s leading hotels with gracious island hospitality, thoughtful amenities and distinctive .</p>\r\n', '', '', 'ourbest.jpg', '', '', 'en', '2019-03-27 09:21:26', '2019-04-10 16:19:36', '', '', '', '', ''),
(23, 0, 0, 'homeourbest', 'homeourbest', 'Unser Bestes', '', '<p>Das Hotel Vista Del Mar ist eines der beliebtesten Hotels von Catalina Island und gilt als eines der f&uuml;hrenden Hotels von Avalon.</p>\r\n', '', '', 'ourbest(1).jpg', '', '', 'de', '2019-03-27 09:22:01', '2019-04-10 16:28:00', '', '', '', '', ''),
(24, 22, 0, 'feature1', 'feature1', '250 Best Rooms 5 Star', '', '', '', '', 'icon-3.png', '', '', 'en', '2019-03-27 09:44:03', '2019-04-10 16:22:28', '', '', '', '', ''),
(25, 22, 0, 'feature2', 'feature2', 'Wet Bar with Refrigerator', '', '', '', '', 'icon-2.png', '', '', 'en', '2019-03-27 09:44:03', '2019-04-10 16:22:40', '', '', '', '', ''),
(26, 22, 0, 'feature3', 'feature3', 'Double Whirlpool Jacuzzi Tub', '', '', '', '', 'icon-4.png', '', '', 'en', '2019-03-28 03:40:34', '2019-04-10 16:22:51', '', '', '', '', ''),
(27, 22, 0, 'feature4', 'feature4', 'Luxurious High Thread Count', '', '', '', '', 'icon-5.png', '', '', 'en', '2019-03-28 03:40:59', '2019-04-10 16:23:00', '', '', '', '', ''),
(28, 22, 0, 'feature5', 'feature5', 'Breakfast each morning', '', '', '', '', 'icon-1.png', '', '', 'en', '2019-03-28 03:41:27', '2019-04-10 16:23:11', '', '', '', '', ''),
(29, 22, 0, 'feature6', 'feature6', 'Ocean Views to lotus Hotel', '', '', '', '', 'icon-6.png', '', '', 'en', '2019-03-28 03:41:56', '2019-04-10 16:23:21', '', '', '', '', ''),
(30, 23, 0, 'feature1', 'feature1', '250 beste Zimmer 5 Sterne', '', '', '', '', 'icon-3(3).png', '', '', 'de', '2019-03-28 03:56:16', '2019-04-10 16:22:34', '', '', '', '', ''),
(31, 23, 0, 'feature2', 'feature2', 'Wet Bar mit Kühlschrank', '', '', '', '', 'icon-2(2).png', '', '', 'de', '2019-03-28 03:58:11', '2019-04-10 16:22:45', '', '', '', '', ''),
(32, 23, 0, 'feature3', 'feature3', 'Doppel-Whirlpool-Whirlpool', '', '', '', '', 'icon-4(4).png', '', '', 'de', '2019-03-28 03:59:18', '2019-04-10 16:22:56', '', '', '', '', ''),
(33, 23, 0, 'feature4', 'feature4', 'Luxuriöse hohe Fadenzahl', '', '', '', '', 'icon-5(5).png', '', '', 'de', '2019-03-28 04:00:31', '2019-04-10 16:23:05', '', '', '', '', ''),
(34, 23, 0, 'feature5', 'feature5', 'Frühstück jeden Morgen', '', '', '', '', 'icon-1(1).png', '', '', 'de', '2019-03-28 04:01:37', '2019-04-10 16:23:14', '', '', '', '', ''),
(35, 23, 0, 'feature6', 'feature6', 'Meerblick auf das Hotel', '', '', '', '', 'icon-6(6).png', '', '', 'de', '2019-03-28 04:02:31', '2019-04-10 16:31:39', '', '', '', '', ''),
(36, 0, 0, 'thevillaabout', 'aboutvillaen', 'About Samari Hill Villas', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'en', '2019-03-29 03:05:33', '2019-04-09 08:45:36', '', '', '', '', ''),
(37, 0, 0, 'thevillaabout', 'aboutvillade', 'Über Samari Hill Villas', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 03:06:34', '2019-04-11 03:48:04', '', '', '', '', ''),
(38, 0, 0, 'thevillaarea', 'areavillaen', 'The area', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'en', '2019-03-29 05:25:28', '2019-04-09 08:45:39', '', '', '', '', ''),
(39, 0, 0, 'thevillaarea', 'areavillade', 'Das Gebiet', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 05:26:01', '2019-04-11 03:48:28', '', '', '', '', ''),
(40, 0, 0, 'thevillabuilding', 'buildingvillaen', 'The building', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'en', '2019-03-29 07:39:22', '2019-04-09 08:45:41', '', '', '', '', ''),
(41, 0, 0, 'thevillabuilding', 'buildingvillade', 'Gebäude', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 07:39:58', '2019-04-11 03:48:52', '', '', '', '', ''),
(43, 0, 0, 'thevillacomfort', 'comfortvillaen', 'Comfort', '', '\r\nComfort\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n', '', '', '', '', '', 'en', '2019-03-29 07:46:02', '2019-04-09 08:45:44', '', '', '', '', ''),
(44, 0, 0, 'thevillacomfort', 'comfortvillade', 'Komfort', '', '<p>Comfort It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'de', '2019-03-29 07:46:30', '2019-04-11 03:49:21', '', '', '', '', ''),
(45, 0, 0, 'thevillaheader', 'headervillaen', 'The Samari Hill Villas', '', '', '', '', '', '', '', 'en', '2019-03-30 03:43:03', '2019-04-09 08:46:08', '', '', '', '', ''),
(46, 0, 0, 'thevillaheader', 'headervillade', 'The Samari Hill Villas de', '', '', '', '', '', '', '', 'de', '2019-03-30 03:43:32', '2019-04-09 08:47:59', '', '', '', '', ''),
(47, 0, 0, 'team_member', '', 'Robet Wiliam', '', 'Manager', 'Manager', '', 'img-3.jpg', '', '', '', '2019-04-01 04:50:57', '2019-04-01 05:13:01', '', 'https://www.facebook.com/', 'https://www.instagram.com', 'https://twitter.com/', 'robert@email.com'),
(48, 0, 0, 'team_member', '', 'JESSICA ALBA', '', 'Founder Hotel', 'Gründer Hotel', '', 'img-4.jpg', '', '', '', '2019-04-01 05:10:52', '2019-04-01 05:14:10', '', 'https://www.facebook.com/', 'https://www.instagram.com', 'https://twitter.com/', 'alba@email.com'),
(49, 0, 0, 'team_member', '', 'Nia', '', 'Staff', 'Mitarbeiter', '', 'img-2.jpg', '', '', '', '2019-04-01 05:15:13', '2019-04-01 05:32:05', '', 'https://www.facebook.com/', 'https://www.instagram.com', 'https://twitter.com/', 'nia@email.com'),
(50, 0, 0, 'team_member', '', 'David Gari', '', 'Staff', 'Mitarbeiter', '', 'img-11.jpg', '', '', '', '2019-04-01 05:16:06', '2019-04-01 05:16:06', '', 'https://www.facebook.com/', 'https://www.instagram.com', 'https://twitter.com/', 'gari@email.com'),
(52, 0, 0, 'locationheader', 'locationheaderen', 'Location Of The Villa', '', '', '', '', '', '', '', 'en', '2019-04-01 08:07:22', '2019-04-09 08:46:13', '', '', '', '', ''),
(53, 0, 0, 'locationheader', 'locationheaderde', 'Lage der Villa', '', '', '', '', '', '', '', 'de', '2019-04-01 08:07:44', '2019-04-09 08:47:53', '', '', '', '', ''),
(54, 0, 0, 'locationabout', 'aboutlocationen', 'Location of the villa', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'en', '2019-04-01 08:52:59', '2019-04-09 08:46:16', '', '', '', '', ''),
(55, 0, 0, 'locationabout', 'aboutlocationde', 'Lage der Villa', '', '<p>it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-04-01 08:53:01', '2019-04-09 08:47:51', '', '', '', '', ''),
(56, 0, 0, 'locationcondition', 'conditionen', 'Directly at the sea', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-01 10:10:27', '2019-04-09 08:46:18', '', '', '', '', ''),
(57, 0, 0, 'locationcondition', 'conditionde', 'Direkt am Meer', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'de', '2019-04-01 10:11:02', '2019-04-09 08:47:49', '', '', '', '', ''),
(58, 0, 0, 'locationmap', 'mapen', 'Far from tourism', '', '<p>Let yourself be enchanted by the uniqueness of the North Balinese coastal region, that is not yet ruined by tourism, and enjoy your stay with all your senses.</p>\r\n', '', '', '', '', '', 'en', '2019-04-02 02:49:01', '2019-04-09 08:46:20', '', '', '', '', ''),
(59, 0, 0, 'locationmap', 'mapde', 'Weit weg vom Tourismus', '', '<p>Let yourself be enchanted by the uniqueness of the North Balinese coastal region, that is not yet ruined by tourism, and enjoy your stay with all your senses.</p>\r\n', '', '', '', '', '', 'de', '2019-04-02 02:49:19', '2019-04-09 08:47:47', '', '', '', '', ''),
(60, 0, 0, 'offersheader', 'offersheaderen', 'Our offers', '', '', '', '', '', '', '', 'en', '2019-04-02 07:51:12', '2019-04-09 08:46:22', '', '', '', '', ''),
(61, 0, 0, 'offersheader', 'offersheaderde', 'Unsere Angebote', '', '', '', '', '', '', '', 'de', '2019-04-02 07:51:44', '2019-04-09 08:47:45', '', '', '', '', ''),
(62, 0, 0, 'offersfirst', 'offersfirsten', 'Relax or discover', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 03:55:10', '2019-04-09 08:46:24', '', '', '', '', ''),
(63, 0, 0, 'offersfirst', 'offersfirstde', 'Entspannen oder entdecken', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'de', '2019-04-04 03:56:03', '2019-04-09 08:47:43', '', '', '', '', ''),
(64, 0, 0, 'offerssecond', 'offersseconden', 'The sky is the limit!', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 04:13:04', '2019-04-09 08:46:26', '', '', '', '', ''),
(65, 0, 0, 'offerssecond', 'offerssecondde', 'Der Himmel ist die Grenze!', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-04 04:13:28', '2019-04-09 08:47:41', '', '', '', '', ''),
(66, 0, 0, 'offersthird', 'offersthirden', 'Food and Drink', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'en', '2019-04-04 04:27:05', '2019-04-09 08:46:28', '', '', '', '', ''),
(67, 0, 0, 'offersthird', 'offersthirdde', 'Essen und Trinken', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 04:27:15', '2019-04-09 08:47:39', '', '', '', '', ''),
(68, 0, 0, 'offersfourth', 'offersfourthen', 'Refreshment', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 04:43:10', '2019-04-09 08:46:30', '', '', '', '', ''),
(69, 0, 0, 'offersfourth', 'offersfourthde', 'Erfrischung', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-04 04:45:14', '2019-04-09 08:47:37', '', '', '', '', ''),
(70, 0, 0, 'offersfifth', 'offersfifthen', 'Tours', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'en', '2019-04-04 04:56:49', '2019-04-09 08:46:32', '', '', '', '', ''),
(71, 0, 0, 'offersfifth', 'offersfifthde', 'Touren', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'de', '2019-04-04 04:57:15', '2019-04-09 08:47:30', '', '', '', '', ''),
(72, 0, 0, 'offerssixth', 'offerssixthen', 'Tours Comfort even on the way', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-04 05:05:31', '2019-04-09 08:46:34', '', '', '', '', ''),
(73, 0, 0, 'offerssixth', 'offerssixthde', 'Touren Komfort auch unterwegs', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-04 05:05:37', '2019-04-09 08:47:28', '', '', '', '', ''),
(74, 0, 0, 'offersseventh', 'offersseventhen', 'Snorkelling and Diving', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'en', '2019-04-04 05:11:25', '2019-04-09 08:46:36', '', '', '', '', ''),
(75, 0, 0, 'offersseventh', 'offersseventhde', 'Schnorcheln und Tauchen', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'de', '2019-04-04 05:11:29', '2019-04-09 08:47:26', '', '', '', '', ''),
(76, 0, 0, 'offerseighth', 'offerseighthen', 'Massage', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-04 05:20:33', '2019-04-09 08:46:39', '', '', '', '', ''),
(77, 0, 0, 'offerseighth', 'offerseighthde', 'Massage', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-04 05:20:37', '2019-04-09 08:47:24', '', '', '', '', ''),
(78, 0, 0, 'pricingheader', 'pricingheaderen', 'Pricing', '', '', '', '', '', '', '', 'en', '2019-04-04 07:33:01', '2019-04-09 08:46:41', '', '', '', '', ''),
(79, 0, 0, 'pricingheader', 'pricingheaderde', 'Die Preise', '', '', '', '', '', '', '', 'de', '2019-04-04 07:33:04', '2019-04-11 04:36:38', '', '', '', '', ''),
(80, 0, 0, 'pricinglist', 'pricinglist', 'Pricelist (download)', 'Preislist1e (download)', '', '', '', 'samari.pdf', '', '', '', '2019-04-04 08:25:51', '2019-04-04 09:10:08', '', '', '', '', ''),
(81, 0, 0, 'pricingfeelingfirst', 'pricingfeelingfirsten', 'Feel comfortable all around', '', '\r\n\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text.\r\n', '', '', '', '', '', 'en', '2019-04-04 08:25:52', '2019-04-09 08:46:44', '', '', '', '', ''),
(82, 0, 0, 'pricingfeelingfirst', 'pricingfeelingfirstde', 'Fühlen Sie sich rundum wohl', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text.</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 09:10:31', '2019-04-09 08:47:19', '', '', '', '', ''),
(83, 0, 0, 'pricingfeelingsecond', 'pricingfeelingseconden', 'Comfort', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-04 09:23:00', '2019-04-09 08:46:46', '', '', '', '', ''),
(84, 0, 0, 'pricingfeelingsecond', 'pricingfeelingsecondde', 'Komfort', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>\r\n', '', '', '', '', '', 'de', '2019-04-04 09:23:05', '2019-04-09 08:47:17', '', '', '', '', ''),
(85, 0, 0, 'galleryheader', 'galleryheaderen', 'Photo impressions', '', '', '', '', '', '', '', 'en', '2019-04-04 10:04:36', '2019-04-09 08:46:48', '', '', '', '', ''),
(86, 0, 0, 'galleryheader', 'galleryheaderde', 'Fotoimpressionen', '', '', '', '', '', '', '', 'de', '2019-04-04 10:05:01', '2019-04-09 08:47:16', '', '', '', '', ''),
(87, 0, 0, 'galleryfirst', 'galleryfirsten', 'Surrounded by palm trees', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing\r\npackages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy.', '', '', '', '', '', 'en', '2019-04-05 02:00:44', '2019-04-09 08:46:50', '', '', '', '', ''),
(88, 0, 0, 'galleryfirst', 'galleryfirstde', 'Umgeben von Palmen', '', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy.</p>\r\n', '', '', '', '', '', 'de', '2019-04-05 02:00:49', '2019-04-09 08:47:13', '', '', '', '', ''),
(89, 0, 0, 'gallerysecond', 'galleryseconden', 'Beautiful places', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'en', '2019-04-05 02:02:17', '2019-04-09 08:46:53', '', '', '', '', ''),
(90, 0, 0, 'gallerysecond', 'gallerysecondde', 'Schöne Orte', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '', '', '', '', '', 'de', '2019-04-05 02:02:50', '2019-04-09 08:47:12', '', '', '', '', ''),
(91, 0, 0, 'gallerythevilla', 'gallerythevilla', 'The Villa', 'die Villa', '', '', '', '', '', '', '', '2019-04-05 03:00:19', '2019-04-05 03:06:46', '', '', '', '', ''),
(92, 0, 0, 'galleryfacets', 'galleryfacets', 'Facets of Bali', 'Facetten von Bali', '', '', '', '', '', '', '', '2019-04-05 03:00:34', '2019-04-05 03:50:52', '', '', '', '', ''),
(93, 0, 0, 'galleryadditional', 'galleryadditional', 'Additional gallery', 'Zusätzliche Galerie', '', '', '', '', '', '', '', '2019-04-05 03:02:38', '2019-04-05 03:02:38', '', '', '', '', ''),
(94, 0, 0, 'guestbookheader', 'guestbookheaderen', 'Our guestbook', '', '', '', '', '', '', '', 'en', '2019-04-05 07:10:12', '2019-04-09 08:46:56', '', '', '', '', ''),
(95, 0, 0, 'guestbookheader', 'guestbookheaderde', 'Unser Gästebuch', '', '', '', '', '', '', '', 'de', '2019-04-05 07:10:17', '2019-04-09 08:47:10', '', '', '', '', ''),
(96, 0, 0, 'guestbookabout', 'guestbookabouten', 'READ OUR GUEST BOOK FORM CUSTOMER', '', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal .', 'WRITE IN GUEST BOOK', '', '', '', '', 'en', '2019-04-05 08:14:32', '2019-04-09 08:46:59', '', '', '', '', ''),
(97, 0, 0, 'guestbookabout', 'guestbookaboutde', 'LESEN SIE UNSEREN GÄSTEBUCH-FORMULAR KUNDEN', '', 'Es ist seit langem bekannt, dass ein Leser beim Blick auf das Layout vom lesbaren Inhalt einer Seite abgelenkt wird. Der Punkt bei der Verwendung von Lorem Ipsum ist, dass es mehr oder weniger normal ist.', 'SCHREIBEN SIE IN GÄSTEBUCH', '', '', '', '', 'de', '2019-04-05 08:14:40', '2019-04-10 01:06:44', '', '', '', '', ''),
(98, 0, 0, 'guestbooklist', '', 'Agus', 'adam@email.com', 'Great Place', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Balo', 'noimage.png', '', '', '', '2019-04-08 02:18:54', '2019-04-10 13:44:37', '', '', '', '', ''),
(99, 0, 0, 'guestbooklist', '', 'Dwi', 'a@a.a', 'Beautiful', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Balo', 'noimage.png', '', '', '', '2019-04-08 02:19:49', '2019-04-10 13:45:31', '', '', '', '', ''),
(100, 0, 0, 'guestbooklist', '', 'yoga', 'yoga@gmail.com', 'Happy', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'bali', 'noimage.png', '', '', '', '2019-04-08 03:28:51', '2019-04-10 13:53:27', 'checked', '', '', '', ''),
(102, 0, 0, 'guestbooklist', '', 'Arim', 'a@email.com', 'Amazing', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Bali', 'noimage.png', '', '', '', '2019-04-08 03:42:01', '2019-04-10 13:53:11', 'checked', '', '', '', ''),
(104, 0, 0, 'guestbooklist', '', 'David Gari', 'david@gmail.com', 'Love', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Jakarta', 'noimage.png', '', '', '', '2019-04-08 03:54:20', '2019-04-10 13:52:59', 'checked', '', '', '', ''),
(105, 0, 0, 'guestbooklist', '', 'Ajosh', 'a@a.a', 'Enjoy', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'England', 'noimage.png', '', '', '', '2019-04-08 03:55:35', '2019-04-10 13:47:08', 'checked', '', '', '', ''),
(106, 0, 0, 'guestbooklist', '', 'Brolyn', 'b@b.com', 'Awesome', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Germany', 'noimage.png', '', '', '', '2019-04-08 03:56:57', '2019-04-10 13:44:52', '', '', '', '', '');
INSERT INTO `tb_general_data` (`general_id`, `general_ref_id`, `oreder`, `general_name`, `general_sub_name`, `general_data`, `general_sub_data`, `general_desc`, `general_sub_desc`, `general_link`, `main_image`, `secondary_image`, `general_password`, `general_lang`, `general_created_at`, `general_updated_at`, `general_additional_info`, `facebook_link`, `instagram_link`, `twitter_link`, `email_link`) VALUES
(107, 0, 0, 'guestbooklist', '', 'arym', 'a@a.a', 'Super Awesome', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Bali', 'noimage.png', '', '', '', '2019-04-08 04:00:15', '2019-04-10 13:44:55', 'checked', '', '', '', ''),
(111, 0, 0, 'guestbooklist', '', 'Nengah', 'n@n.com', 'Special moment', 'Both the outstanding staff and the beautiful room made our first visit to Samari Hill Villas such a success! We enjoyed the appetizers during "wine time", the turndown service, the fresh flowers in our room and the breakfast delivered to our room in a wicker basket.. An attendant set it out for us in a charming fashion. We would not consider another property when we return to Samari Hill Villas!', 'Madrid', 'aj.jpg', '', '', '', '2019-04-09 08:17:10', '2019-04-10 13:54:14', 'checked', '', '', '', ''),
(112, 0, 0, 'guestbookform', 'guestbookformen', 'GIVE ME YOU THING WITH US', 'Your Name', 'Your feedback means the world to us, it''s how we improve our level of service. Feel free to share your experience if you''ve stayed with us before.', 'Locations (Town / Country)', 'Testimonial Title', 'Your Testimonial', 'Profil Picture', 'WRITE IN GUEST BOOK', 'en', '2019-04-10 06:24:43', '2019-04-10 06:43:16', '', '', '', '', ''),
(113, 0, 0, 'guestbookform', 'guestbookformde', 'Gib mir was mit uns', 'Dein Name', 'Ihr Feedback bedeutet für uns die Welt, wie wir unser Serviceniveau verbessern. Teilen Sie uns Ihre Erfahrungen mit, wenn Sie zuvor bei uns geblieben sind.', 'Standorte (Stadt / Land)', 'Testimonial Titel', 'Ihr Zeugnis', 'Profilbild', 'SCHREIBEN SIE IN GÄSTEBUCH', 'de', '2019-04-10 06:24:49', '2019-04-10 06:29:24', '', '', '', '', ''),
(114, 0, 0, 'contactheader', 'contactheaderen', 'Contact Us', '', '', '', '', '', '', '', 'en', '2019-04-10 07:01:06', '2019-04-10 07:12:01', '', '', '', '', ''),
(115, 0, 0, 'contactheader', 'contactheaderde', 'Kontaktiere uns', '', '', '', '', '', '', '', 'de', '2019-04-10 07:01:16', '2019-04-10 07:02:03', '', '', '', '', ''),
(116, 0, 0, 'reservation', 'reservationen', 'YOUR RESERVATION', 'Name', 'Email', 'Phone number', 'Arrival', 'Departure', 'Guests', 'Message', 'en', '2019-04-10 08:47:39', '2019-04-10 09:17:28', 'Submit request', '', '', '', ''),
(117, 0, 0, 'reservation', 'reservationde', 'DEINE RESERVIERUNG', 'Name', 'E-mail', 'Telefonnummer', 'Anreise', 'Abreise', 'Anzahl Gäste', 'Nachricht', 'de', '2019-04-10 08:47:51', '2019-04-10 09:38:13', 'Anfrage senden', '', '', '', ''),
(118, 0, 0, 'contactcontent', 'contactcontenten', 'Do you have any questions? We are happy to help!', 'Location on Bali', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>\r\n', '', '', '', '', 'en', '2019-04-10 09:26:19', '2019-04-10 09:36:16', '', '', '', '', ''),
(119, 0, 0, 'contactcontent', 'contactcontentde', 'Sie haben noch Fragen? Wir helfen gerne!', 'Lage auf Bali', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.', '', '', '', '', 'de', '2019-04-10 09:26:31', '2019-04-10 09:28:50', '', '', '', '', ''),
(120, 0, 0, 'availabilityheader', 'availabilityheaderen', 'Samari Hill Villas Availability', '', '', '', '', '', '', '', 'en', '2019-04-11 08:06:12', '2019-04-11 08:19:52', '', '', '', '', ''),
(121, 0, 0, 'availabilityheader', 'availabilityheaderde', 'Belegungsplan Samari Hill Villas', '', '', '', '', '', '', '', 'de', '2019-04-11 08:06:22', '2019-04-11 08:19:17', '', '', '', '', ''),
(122, 0, 0, 'headerfooter', 'phone2', '+62 812-1234-5678', '', '', '', '', '', '', '', '', '2019-04-23 03:11:22', '2019-04-23 03:12:30', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kamus`
--

CREATE TABLE `tb_kamus` (
  `id_kamus` int(9) NOT NULL,
  `id_lang` varchar(200) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `title` varchar(250) NOT NULL,
  `param` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kamus`
--

INSERT INTO `tb_kamus` (`id_kamus`, `id_lang`, `kategori`, `title`, `param`, `created_at`, `updated_at`) VALUES
(1, 'en', 'viewdetail', 'View Detail', 'viewdetailen', '2019-03-25 07:29:19', '2019-04-22 02:20:30'),
(2, 'de', 'viewdetail', 'Im Detail sehen', 'viewdetailde', '2019-04-11 01:18:03', '2019-04-22 02:20:30'),
(3, 'en', 'home', 'Home', 'homeen', '2019-04-11 01:35:59', '2019-04-11 03:23:47'),
(4, 'de', 'home', 'Das Haus', 'homede', '2019-04-11 01:36:32', '2019-04-11 03:23:47'),
(5, 'en', 'thevilla', 'The Villa', 'thevillaen', '2019-04-11 01:40:12', '2019-04-11 03:23:47'),
(6, 'de', 'thevilla', 'Die Villa', 'thevillade', '2019-04-11 01:40:58', '2019-04-11 03:23:47'),
(7, 'en', 'location', 'Location', 'locationen', '2019-04-11 01:43:25', '2019-04-11 03:23:47'),
(8, 'de', 'location', 'Lage', 'locationde', '2019-04-11 01:43:40', '2019-04-11 03:23:48'),
(9, 'en', 'offers', 'Offers', 'offersen', '2019-04-11 01:46:28', '2019-04-11 03:23:48'),
(10, 'de', 'offers', 'Angebote', 'offersde', '2019-04-11 01:48:28', '2019-04-11 03:23:48'),
(11, 'en', 'gallery', 'Gallery', 'galleryen', '2019-04-11 01:49:38', '2019-04-11 03:23:48'),
(12, 'de', 'gallery', 'Galerie', 'galleryde', '2019-04-11 01:50:26', '2019-04-11 03:23:48'),
(13, 'en', 'availability', 'Availability', 'availabilityen', '2019-04-11 01:52:40', '2019-04-11 03:23:48'),
(14, 'de', 'availability', 'Belegung', 'availabilityde', '2019-04-11 01:53:06', '2019-04-11 03:23:48'),
(15, 'en', 'guestbook', 'Guestbook', 'guestbooken', '2019-04-11 01:54:49', '2019-04-11 03:23:48'),
(16, 'de', 'guestbook', 'Gästebuch', 'guestbookde', '2019-04-11 01:55:06', '2019-04-11 03:23:48'),
(17, 'en', 'pricing', 'Pricing', 'pricingen', '2019-04-11 01:57:49', '2019-04-11 03:23:48'),
(18, 'de', 'pricing', 'Preise', 'pricingde', '2019-04-11 01:58:08', '2019-04-11 03:23:49'),
(19, 'en', 'contact', 'Contact', 'contacten', '2019-04-11 01:59:42', '2019-04-11 03:23:49'),
(20, 'de', 'contact', 'Kontakt', 'contactde', '2019-04-11 02:00:07', '2019-04-11 03:23:49'),
(21, 'en', 'teammember', 'Team Member', 'teammemberen', '2019-04-11 03:52:02', '2019-04-22 02:20:30'),
(22, 'de', 'teammember', 'Teammitglied', 'teammemberde', '2019-04-11 03:52:28', '2019-04-11 03:54:54'),
(23, 'en', 'groundplan', 'Groundplan', 'groundplanen', '2019-04-22 02:22:13', '2019-04-22 02:26:23'),
(24, 'de', 'groundplan', 'Grundriss', 'groundplande', '2019-04-22 02:23:23', '2019-04-22 02:26:23');

-- --------------------------------------------------------

--
-- Table structure for table `tb_language`
--

CREATE TABLE `tb_language` (
  `id_lang` int(9) NOT NULL,
  `code_lang` varchar(50) NOT NULL,
  `language` varchar(50) NOT NULL,
  `lang_word` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_language`
--

INSERT INTO `tb_language` (`id_lang`, `code_lang`, `language`, `lang_word`) VALUES
(1, 'de', 'Deutsch (German)', 'deutsch(german)'),
(2, 'en', 'English (Englisch)', 'english(englisch)');

-- --------------------------------------------------------

--
-- Table structure for table `tb_location`
--

CREATE TABLE `tb_location` (
  `id_location` int(9) NOT NULL,
  `name` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_location`
--

INSERT INTO `tb_location` (`id_location`, `name`, `latitude`, `longitude`) VALUES
(1, 'Samari Hill Villas', '-8.096309060950142', '115.25496058166027');

-- --------------------------------------------------------

--
-- Table structure for table `tb_picture`
--

CREATE TABLE `tb_picture` (
  `general_id` int(9) NOT NULL,
  `general_ref_id` varchar(200) NOT NULL,
  `picture_name` varchar(200) NOT NULL,
  `second_picture` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_picture`
--

INSERT INTO `tb_picture` (`general_id`, `general_ref_id`, `picture_name`, `second_picture`) VALUES
(3, 'the_area', 'th.jpg', ''),
(4, 'the_area', 'th2.jpg', ''),
(5, 'the_building', 'buil2.jpg', ''),
(6, 'the_building', 'buil.jpg', ''),
(7, 'comfort', 'com.jpg', ''),
(8, 'comfort', 'com(1).jpg', ''),
(13, 'header_villa', 'man1.jpg', ''),
(14, 'home_menulink1', 'Discover-Bali.jpg', ''),
(15, 'header_location', 'locheader.jpg', ''),
(18, 'about_villa', 'ab2.jpg', ''),
(19, 'about_villa', 'ab.jpg', ''),
(20, 'about_location', 'abloc2.jpg', ''),
(21, 'about_location', 'abloc.jpg', ''),
(22, 'condition', 'cond2.jpg', ''),
(23, 'condition', 'cond.jpg', ''),
(24, 'header_offers', 'offe1.jpg', ''),
(25, 'offers_first', 'off1.jpg', ''),
(26, 'offers_first', 'off2.jpg', ''),
(27, 'offers_second', 'off21.jpg', ''),
(28, 'offers_third', 'off3(1)(1).jpg', ''),
(29, 'offers_third', 'off3_(2).jpg', ''),
(30, 'offers_fourth', 'off4.jpg', ''),
(32, 'offers_sixth', 'off6.jpg', ''),
(33, 'offers_seventh', 'off7.jpg', ''),
(34, 'offers_seventh', 'off7(1).jpg', ''),
(35, 'offers_eighth', 'off8.jpg', ''),
(36, 'offers_fifth', 'off5a.jpg', ''),
(37, 'header_pricing', 'pric.jpg', ''),
(38, 'first_feeling', 'pricf1.jpg', ''),
(39, 'second_feeling', 'pricf2.jpg', ''),
(40, 'header_gallery', 'gal1.jpg', ''),
(42, 'gallery_thevilla', 'ab.jpg', 'ab_thumb.jpg'),
(43, 'gallery_thevilla', 'gale.jpg', 'gale_thumb.jpg'),
(44, 'gallery_thevilla', 'th2.jpg', 'th2_thumb.jpg'),
(45, 'gallery_facets', 'pura_agung_blambanga_2.jpg', 'pura_agung_blambanga_2_thumb.jpg'),
(46, 'gallery_facets', 'barong-dance-10.jpg', 'barong-dance-10_thumb.jpg'),
(47, 'gallery_facets', 'GWK-1.jpg', 'GWK-1_thumb.jpg'),
(48, 'gallery_additional', 'off3(1)(1).jpg', 'off3(1)(1)_thumb.jpg'),
(49, 'gallery_additional', 'off5a.jpg', 'off5a_thumb.jpg'),
(50, 'header_guestbook', 'gues.jpg', ''),
(51, 'guestbook_about', 'img-8.jpg', ''),
(52, 'homemenulink1', 'Discover-Bali.jpg', ''),
(53, 'homemenulink2', 'Experience-culture1.jpg', ''),
(54, 'homemenulink3', 'Sense-relaxation.jpg', ''),
(55, 'header_contact', 'cont.jpg', ''),
(56, 'header_contact', 'cont.jpg', ''),
(57, 'guestbook_gallery', 'guestbook-11.jpg', ''),
(58, 'guestbook_gallery', 'guestbook-21.jpg', ''),
(61, 'homeaboutus', 'aboutus.jpg', ''),
(65, 'homeourbest', 'ourbest.jpg', ''),
(67, 'feature1', 'icon-3.png', ''),
(68, 'feature2', 'icon-2.png', ''),
(69, 'feature3', 'icon-4.png', ''),
(70, 'feature4', 'icon-5.png', ''),
(71, 'feature5', 'icon-1.png', ''),
(72, 'feature6', 'icon-6.png', ''),
(73, 'header_availability', 'avai1.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_reservasi`
--

CREATE TABLE `tb_reservasi` (
  `id_reservasi` int(9) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `guest` varchar(10) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_reservasi`
--

INSERT INTO `tb_reservasi` (`id_reservasi`, `name`, `email`, `phone`, `start`, `end`, `guest`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Agus Arym', 'a@a.a', '085792648170', '2019-04-16', '2019-04-20', '1-2', '', '2019-04-16 07:31:41', '2019-04-16 07:31:41'),
(2, 'Bondan Noviada', 'b@b.com', '+62 878-6080-65', '2019-04-20', '2019-04-24', '5-10', 'Pick up at Terminal Ubung ', '2019-04-16 07:36:49', '2019-04-16 07:36:49'),
(3, 'Yoga Apriadi', 'yoga@gmail.com', '+62 878-6080-65', '2019-04-30', '2019-05-03', '5-10', '', '2019-04-16 07:37:40', '2019-04-16 07:37:40'),
(4, 'Brolyn Ajosh', 'b@b.com', '085792648170', '2019-05-15', '2019-05-16', '1-2', 'Pick up at air port', '2019-04-18 06:18:35', '2019-04-18 06:18:35'),
(5, 'David Gari', 'davidg@gmail.com', '+62 878-6080-651', '2019-06-16', '2019-06-23', '5-10', 'Dont be arogan', '2019-04-18 06:19:40', '2019-04-18 06:24:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_social_link`
--

CREATE TABLE `tb_social_link` (
  `id_social_link` int(9) NOT NULL,
  `social_name` varchar(300) NOT NULL,
  `social_link` varchar(300) NOT NULL,
  `social_user_id` int(9) NOT NULL,
  `social_logo` varchar(50) NOT NULL,
  `social_desc` text NOT NULL,
  `published` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_social_link`
--

INSERT INTO `tb_social_link` (`id_social_link`, `social_name`, `social_link`, `social_user_id`, `social_logo`, `social_desc`, `published`, `created_at`, `updated_at`) VALUES
(1, 'Facebook', 'https://www.facebook.com/', 0, 'fa fa-facebook', '', 'checked', '2019-03-13 07:50:26', '2019-03-13 09:26:43'),
(2, 'Instagram', 'https://www.instagram.com/', 0, 'fa fa-instagram', '', 'checked', '2019-03-13 07:50:26', '2019-03-13 09:27:38'),
(3, 'Twitter', 'https://twitter.com/', 0, 'fa fa-twitter', '', 'checked', '2019-03-13 07:50:26', '2019-03-13 09:28:28'),
(4, 'Youtube', 'https://www.youtube.com/', 0, 'fa fa-youtube', '', '', '2019-03-13 07:50:26', '2019-04-08 02:25:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(9) NOT NULL,
  `name` varchar(300) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `type` varchar(100) NOT NULL,
  `remember_token` varchar(255) NOT NULL,
  `created_token` date NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `name`, `email`, `password`, `type`, `remember_token`, `created_token`, `image`, `created_at`, `updated_at`, `user_desc`) VALUES
(1, 'Administrator Samari Hill Villas', 'agusdownload5293@gmail.com', 'siadmin1234', 'admin', '5fff1b292a0804a0c79d748d59b49a', '2019-04-12', 'follower-avatar.jpg', '2019-04-12 02:49:50', '2019-04-12 03:02:39', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`artikel_id`);

--
-- Indexes for table `tb_day`
--
ALTER TABLE `tb_day`
  ADD PRIMARY KEY (`id_day`);

--
-- Indexes for table `tb_general_data`
--
ALTER TABLE `tb_general_data`
  ADD PRIMARY KEY (`general_id`);

--
-- Indexes for table `tb_kamus`
--
ALTER TABLE `tb_kamus`
  ADD PRIMARY KEY (`id_kamus`);

--
-- Indexes for table `tb_language`
--
ALTER TABLE `tb_language`
  ADD PRIMARY KEY (`id_lang`);

--
-- Indexes for table `tb_location`
--
ALTER TABLE `tb_location`
  ADD PRIMARY KEY (`id_location`);

--
-- Indexes for table `tb_picture`
--
ALTER TABLE `tb_picture`
  ADD PRIMARY KEY (`general_id`);

--
-- Indexes for table `tb_reservasi`
--
ALTER TABLE `tb_reservasi`
  ADD PRIMARY KEY (`id_reservasi`);

--
-- Indexes for table `tb_social_link`
--
ALTER TABLE `tb_social_link`
  ADD PRIMARY KEY (`id_social_link`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_day`
--
ALTER TABLE `tb_day`
  MODIFY `id_day` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `tb_general_data`
--
ALTER TABLE `tb_general_data`
  MODIFY `general_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `tb_kamus`
--
ALTER TABLE `tb_kamus`
  MODIFY `id_kamus` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_language`
--
ALTER TABLE `tb_language`
  MODIFY `id_lang` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_location`
--
ALTER TABLE `tb_location`
  MODIFY `id_location` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_picture`
--
ALTER TABLE `tb_picture`
  MODIFY `general_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `tb_reservasi`
--
ALTER TABLE `tb_reservasi`
  MODIFY `id_reservasi` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_social_link`
--
ALTER TABLE `tb_social_link`
  MODIFY `id_social_link` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
